export const environment = {
  production: false,
  apiUrl: 'http://scripter:8000/api/',
  cookieName: 'apitoken',
  tokenName: 'apitoken',
  base: '/',
  delayTokenCheck: 10000, // задержка по времни для проверки автризации в милисекундах
};
