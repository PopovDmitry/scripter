export const environment = {
  production: true,
  apiUrl: 'http://82.202.212.77/api/',
  cookieName: '_at',
  tokenName: '_at',
  base: '/app',
  delayTokenCheck: 1000 * 60 * 10, // задержка по времни для проверки автризации
};
