import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class ScenarioService extends ApiService {

  /**
   * Получаем по номеру старницы страницу :) 
   * @param page номер страницы
   */
  getScenarios(page: number = 1): Promise<any>{
    return this.get('scenario?page=' + page);
  }

  /**
   * Получение сценария
   * @param id ID сценария
   */
  getScenario(id: number): Promise<any>{
    return this.get('scenario/view/' + id);
  }

  /**
   * Копироание сценария
   * @param id ID сценария
   */
  copyScenario(id: number): Promise<any>{
    return this.get('scenario/copy/' + id);
  }

  /**
   * Удаление сценария
   * @param id ID сценария
   */
  deleteScenario(id: number): Promise<any>{
    return this.delete('scenario/' + id);
  }

  /**
   * создание сценария
   * @param name название сценария
   */
  createScenario(name: string): Promise<any>{
    return this.post('scenario', {name});
  }

  /**
   * обновление сценария
   * @param name имя сценария
   * @param id ID сценария
   */
  updateScenario(name: string, id: number): Promise<any>{
    return this.put('scenario/' + id, {name});
  }
    

}
