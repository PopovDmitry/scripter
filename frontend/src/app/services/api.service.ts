import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import { Observable, Subject } from "rxjs/Rx";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { AuthService } from './auth.service';

@Injectable()
export class ApiService extends AuthService{

  private serverUrl: string = environment.apiUrl;
  public token: string;
  public isDisplayNotification: boolean = false;
  

  /**
   * Удаление токена
   */
  deleteToken(): void{
    if(this.token != undefined){
      this.token = undefined;
      if(this.cookieService.check(environment.cookieName)){
        this.cookieService.delete(environment.cookieName)
      }
    }
  }


  /**
   * Получение GET запросов
   * @param method API метод
   */
  get(method): Promise<any>{
    return new Promise( (resolve, reject) => {
      this
        .http
        .get( this.serverUrl + method, this.createRequestOptions() )
        .map((res: Response) => {
          return res.json();
        })
        .toPromise()
        .then( (success) => {
          resolve(success);
        }).catch( e => {
            reject(e);
        })
    });
  }

  /**
   * Отправка методом POST
   * @param method API метод
   * @param params Параметры для отправки
   */
  post( method: string, params: any ): Promise<any> {
    return new Promise( (resolve, reject) => {
      this
        .http
        .post( this.serverUrl + method, params, this.createRequestOptions() )
        .map((res: Response) => {
          return res.json();
        })
        .toPromise()
        .then( (success) => {
          resolve(success);
        }).catch( e => {
            reject(e);
        })
    });
  }

  /**
   * Отправка методом PUT
   * @param method API метод
   * @param params Параметры для отправки
   */
  put( method: string, params: any ): Promise<any> {
    return new Promise( (resolve, reject) => {
      this
        .http
        .put( this.serverUrl + method, params, this.createRequestOptions() )
        .map((res: Response) => {
          return res.json();
        })
        .toPromise()
        .then( (success) => {
          resolve(success);
        }).catch( e => {
            reject(e);
        })
    });
  }

  /**
   * Отправка методом DELETE
   * @param method API метод
   * @param params данные которые уходят на сервер
   */
  delete(method: string): Promise<any>{
    return new Promise( (resolve, reject) => {
      this
        .http
        .delete( this.serverUrl + method, this.createRequestOptions() )
        .map((res: Response) => {
          return res.json();
        })
        .toPromise()
        .then( (success) => {
          resolve(success);
        }).catch( e => {
            this.handleError(e);
            reject(e);
        })
    });
  }

  /**
   * Установка заголовков
   */
  createRequestOptions(): any {
    let headers = new Headers(); 
    if(this.token != undefined){
      headers.append("Authorization", "Bearer " + this.token);
    }
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return options;
  }

  
  /**
   * Обработка ошибки
   * @param errorHttp код ошибки
   */
  handleError (errorHttp: Response | any) {
    this.notificationsService.alert
    var errMsg;
    if (errorHttp instanceof Response) {
      errMsg = errorHttp.json() || '';
    } else {
      errMsg = errorHttp.message ? errorHttp : errorHttp.toString();
    }

    
    if(errorHttp.status && this.isDisplayNotification){
      switch (errorHttp.status) {
        case 500:
          this.notificationsService.error(
            '500',
            errMsg.message
          )
          break;
        case 422:
          this.notificationsService.warn(
            '422',
            errMsg.message
          )
          break;
        case 401:
          this.notificationsService.alert(
            '401',
            errMsg.message
          )
          break;
      
        default:
          break;
      }
    } else {

    }

    console.log(errorHttp.status)
    // this.notificationsService.error()

  }

}
