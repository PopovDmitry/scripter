import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class GroupService extends ApiService {

  /**
   * получение групп
   */
  getAllGroups(): Promise<any>{
    return this.get('groups')
  }

  /**
   * Создание группы
   * @param name 
   * @param parent_id 
   */
  createGroup(name: string, parent_id: string): Promise<any>{
    return this.post('groups', {name, parent_id})
  }

  /**
   * копирование корневой группы
   * @param id 
   */
  cloneGroup(id: number): Promise<any>{
    return this.get('groups/copyAsRoot/' + id);
  }


  /**
   * копирование дочерний группы группы
   * @param id 
   */
  copyTreeById(id: number): Promise<any>{
    return this.get('groups/copyTreeById/' + id);
  }


  /**
   * обновление группы
   * @param id 
   */
  updateGroup(id: number, name: string, parent_id: number, sort: number): Promise<any>{
    return this.put('groups/' + id, {name, parent_id, sort});
  }

  /**
   * удаление группы
   * @param id 
   */
  deleteGroup(id: number): Promise<any>{
    return this.delete('groups/' + id);
  }

  /**
   * получение информации о сценарии
   * @param id 
   */
  getScenarioData(id: number): Promise<any>{
    return this.get('groups/scenario/' + id);
  }

  /**
   * Созадние связи группы и сценария
   * @param group_id 
   * @param scenario_id 
   */
  addRealtion(group_id: number, scenario_id: number): Promise<any>{
    return this.post('groups/relation', {
      group_id, scenario_id
    })
  }

  deleteRealtion(group_id: number, scenario_id: number): Promise<any>{
    return this.delete('groups/relation/' + scenario_id + '/' + group_id);
  }

}
