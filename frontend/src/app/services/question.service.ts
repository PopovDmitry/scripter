import { Injectable } from '@angular/core';
import { ApiService } from './api.service';


@Injectable()
export class QuestionService extends ApiService {

  /**
   * получение вопросов по группе
   * @param group_id id группы
   */
  getQuestions(group_id: number): Promise<any>{
    return this.get('question/group/' + group_id)
  }

  /**
   * Удаление группы
   * @param id ID вопроса
   */
  deleteQuestion(id: number ): Promise<any>{
    return this.delete('question/' + id);
  }

  /**
   * Создание группы
   * @param group_id ID группы
   * @param question Вопрос
   * @param answers Ответы
   */
  createQuestion(group_id: number, question: string, answers: string[], sort: number): Promise<any>{
    return this.post('question', {
      group_id,
      question,
      answers,
      sort,
    })
  }

  /**
   * Получение вопроса
   * @param id ID вопроса
   */
  viewQuestion(id: number): Promise<any>{
    return this.get('question/view/' + id);
  }

  /**
   * Обновление 
   * @param id ID вопроса
   * @param question вопрос
   * @param answers ответы в массиве
   * @param sort сортировка
   */
  updateQuestion(id: number, question: string, answers: string[], sort: number): Promise<any>{
    return this.put('question/' + id, {
      question,
      answers,
      sort,
    })
  }

  /**
   * получение максимального значения сортировки в вопросах
   * @param group_id ID группы
   */
  getMaxSort(group_id: number): Promise<any>{
    return this.get('question/getMaxSort/' + group_id)
  }

  /**
   * Копирование вопроса
   * @param id ID вопроса
   */
  copyQuestion(id: number): Promise<any>{
    return this.get('question/copyQuestion/' + id)
  }

  /**
   * получение списка групп вопросов к ним
   * @param group_id 
   */
  getAllQuestionsByGroupId(group_id: number){
    return this.get('question/group/all/' + group_id);
  }

}
