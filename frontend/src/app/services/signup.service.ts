import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class SignupService extends ApiService {

  // constructor() { }

  /**
   * Регистрация нового пользователя
   * @param name Имя пользователя
   * @param email Email пользователя
   * @param password Пароль пользователя
   */
  signUp(name: string, email: string, password: string, company: string): Promise<any>{
    return new Promise( (resolve, reject)=> {
      this.post('signup', {email, password, name, company})
      .then( success => {
        if(success.token != undefined){
          // this.setToken(success.token);
          resolve(true);
        } else {
          resolve(true);
        }
      } )
      .catch( error => {
        console.error('API ERROR', error );
        reject(error)
      })
    });
  }

}
