import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class ManagerService extends ApiService {

  /**
   * Получение списка менеджеров
   * @param page 
   */
  getManagers(page: number = 1): Promise<any>{
    return this.get('manager?page=' + page);
  }

  /**
   * Удаление менеджера
   * @param email адрес электронной почты
   */
  deleteManager(email: string): Promise<any>{
    return this.delete('manager/' + email);
  }

  /**
   * Добавление менеджера
   * @param email адрес электронный почты
   * @param name Имя менеджера
   * @param password Пароль менеджера
   */
  addManager(email: string, name: string, password: string): Promise<any>{
    return this.post('manager', {email, name, password});
  }
}
