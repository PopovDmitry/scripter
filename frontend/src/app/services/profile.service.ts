import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class ProfileService extends ApiService {

  /**
   * Получение текущего профайла
   */
  getProfile(): Promise<any>{
    return this.get('profile');
  }

  /**
   * Сохранение профиля
   * @param saveData данные из формы
   */
  updateProfile(saveData: any): Promise<any>{
    return this.put('profile', saveData);
  }
}
