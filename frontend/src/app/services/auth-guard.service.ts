import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthGuard implements CanActivate {

  private lastUpdate: number = 0;

  constructor(private auth: AuthService, private router: Router) { }

  canActivatePromise(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.auth.authenticated) {
        reject(false);
      } else {
        resolve(true);
      }
    })
  }

  canActivate(){
    // проверка на авторизацию
    if (!this.auth.authenticated) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}