import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { JwtHelper, tokenNotExpired } from './helpers/jwt.helper';
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { CookieService } from 'ngx-cookie-service';

import { NotificationsService } from 'angular2-notifications';


@Injectable()
export class AuthService {
  protected token: string;
  /**
   * индикатор авторизации
   */
  loggedIn: boolean;
  loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);

  constructor(
    protected router: Router,
    protected http: Http,
    protected cookieService: CookieService,
    public notificationsService: NotificationsService
  ) {
    if (this.authenticated) {
      if(this.getToken() == ''){
        this.setLoggedIn(false);
        this.router.navigate(['/login']);
      } else{
        this.setLoggedIn(true);
        this.token = this.getToken();
      }
    } else {
      this.setLoggedIn(false);
    }
  }

  /**
   * Устанавливает индикатор авторизации
   * @param value указаетль активности
   */
  public setLoggedIn(value: boolean) {
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  /**
   * Авторизация в api
   * @param email email пользоваетеля
   * @param password пароль пользователя
   */
  public login(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this
        .http
        .post(environment.apiUrl + 'login', { email, password })
        .map((res: Response) => {
          return res.json();
        })
        .toPromise()
        .then((success) => {
          this.setToken(success.token);
          // отмечаем успешность входа
          // this.setLoggedIn(true);

          // перенаправляемы
          this.router.navigate(['/profile']);
          resolve(true)
        })
        .catch(err => {
          reject(err);
        })
    })
  }

  public getStatus(): Observable<boolean>{
    return this.loggedIn$.asObservable();
  }

  /**
   * Выход
   */
  public logout(): void {
    localStorage.removeItem(environment.tokenName);
    this.router.navigate(['/login']);
    this.setLoggedIn(false);
  }

  /**
   * проверяет не истек ли ключ доступа
   */
  get authenticated(): boolean {
    return tokenNotExpired(environment.tokenName);
  }

  /**
   * Установка токена
   * @param token токен
   */
  protected setToken(token: string): void {
    localStorage.setItem(environment.tokenName, token);
    this.token = token;
    
  }

  /**
   * полученеи токена
   */
  protected getToken(): string {
    // var helper = new JwtHelper
    if (this.authenticated) {
      return this.token || localStorage.getItem(environment.tokenName)
    } else {
      return '';
    }
  }
}
