import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, HttpModule, JsonpModule } from '@angular/http';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TreeModule } from 'angular-tree-component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';


import { APP_BASE_HREF } from '@angular/common';
import { environment } from '../environments/environment';


import { CookieService } from 'ngx-cookie-service';
import { ApiService } from "./services/api.service";
import { AuthService } from "./services/auth.service";
import { AuthGuard } from "./services/auth-guard.service";
import { LoginService } from "./services/login.service";
import { SignupService } from "./services/signup.service";
import { ProfileService } from "./services/profile.service";
import { ManagerService } from "./services/manager.service";
import { ScenarioService } from "./services/scenario.service";
import { GroupService } from "./services/group.service";
import { QuestionService } from "./services/question.service";

import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/template/header/header.component';
import { FooterComponent } from './components/template/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ModalComponent } from './components/modal/modal.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ScenarioComponent } from './components/scenario/scenario.component';
import { FormComponent } from './components/scenario/form/form.component';
import { GroupComponent } from './components/group/group.component';
import { QuestionComponent } from './components/question/question.component';
import { WorkplaceComponent } from './components/workplace/workplace.component';
import { NewWorkplaceComponent } from './components/new-workplace/new-workplace.component';
import { KeepHtmlPipe } from './keep-html.pipe';
import { NewDisplayChildrenGroupsComponent } from './components/new-display-children-groups/new-display-children-groups.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    DashboardComponent,
    ModalComponent,
    ScenarioComponent,
    FormComponent,
    GroupComponent,
    QuestionComponent,
    WorkplaceComponent,
    NewWorkplaceComponent,
    KeepHtmlPipe,
    NewDisplayChildrenGroupsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    routing,
    SimpleNotificationsModule.forRoot(),
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    TreeModule,
    ScrollToModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [
    CookieService,
    ApiService,
    AuthService,
    AuthGuard,
    LoginService,
    SignupService,
    ProfileService,
    ManagerService,
    ScenarioService,
    GroupService,
    QuestionService
    // {
    //   provide: APP_BASE_HREF,
    //   useValue: environment.base
    // }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalComponent
  ]
})
export class AppModule { }
