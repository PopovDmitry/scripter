import { Component } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private _service: NotificationsService) {}

  public options = {
      position: ["top", "right"],
      timeOut: 5000,
      lastOnBottom: true,
      clickToClose: true,
      showProgressBar: true,
      maxLength: 100
  };
}
