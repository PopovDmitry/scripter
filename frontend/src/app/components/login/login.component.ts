import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
// import { AuthService } from '../../services/auth.service';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /**
   * Группа формы
   */
  public LoginForm: FormGroup;
  
  /**
   * индикатор ошибки при отправке формы
   */
  public fromError: boolean = false;

  /**
   * индикатор загрузки
   */
  public isLoading: boolean = false;

  public errors: string[];
  public errorMessage: string = '';

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    // public authService: AuthService,
    public apiService: ApiService,
    private router: Router
  ) { 
    this.LoginForm = fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]]
    })
  }

  /**
   * ОТправка данных и формы
   * 
   * @param formData данные из формы
   */
  submit(formData: any){
    this.fromError = false;
    this.isLoading = true;
    this.errors = [];
    this.errorMessage = '';

    this
      .apiService
      .login(formData.email, formData.password)
      .then( success =>{
        this.fromError = false;
        this.router.navigate(['/']);
        this.apiService.setLoggedIn(true);
      })
      .catch( e => {
        var ee = e.json();
        this.errors = ee.errors;
        this.errorMessage = ee.message;
        this.fromError = true;
        this.isLoading = false;
      })
  }

  ngOnInit() {
  }

}
