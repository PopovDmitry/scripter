import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { SignupService } from '../../services/signup.service';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  /**
   * Группа формы
   */
  public SignupForm: FormGroup;

  /**
   * индикатор ошибки при отправке формы
   */
  public fromError: boolean = false;
  
  /**
   * индикатор загрузки
   */
  public isLoading: boolean = false;

  /**
   * список ошибок
   */
  public errors: string[];

  /**
   * Основная ошибка от сервера
   */
  public errorMessage: string = '';
  
  public displayRegMSG: boolean = false;

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    public signupService: SignupService,
    public apiService: ApiService,
    private router: Router,
    private modalService: NgbModal
  ) { 
    this.SignupForm = fb.group({
      name: [null],
      email: [null, [Validators.required, Validators.email]],
      company: [null],
      password: [null, [Validators.required, Validators.minLength(6)]],
      allow: [false, [Validators.requiredTrue]],
    })
  }

  /**
   * ОТправка данных и формы
   * 
   * @param formData данные из формы
   */
  submit(formData: any){
    this.fromError = false;
    this.isLoading = true;
    this.errors = [];
    this.errorMessage = '';

    this
      .signupService
      .signUp(formData.name, formData.email, formData.password, formData.company)
      .then( success =>{
        this.fromError = false;
        this.displayRegMSG = true;
        this.isLoading = false;
        console.log('dd');
        
        // this.router.navigate(['/profile']);
        // this.apiService.setLoggedIn(true);
      })
      .catch( e => {
        var ee = e.json();
        this.errorMessage = ee.message;
        for(var err in ee.errors){
          var el: any = ee.errors[err]
            el.forEach(element => {
              this.errors.push(element)
            });
        }
        
        this.fromError = true;
        this.isLoading = false;
      })
  }

  /**
   * Открытие модального окна
   * @param content шаблон
   */
  openModal(content){
    this.modalService.open(content).result.then((result) => {
      console.log(`Closed with: ${result}`);  
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  ngOnInit() {
  }

}
