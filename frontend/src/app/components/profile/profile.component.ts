import { Component, OnInit, Inject, Input, ViewContainerRef, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '../../services/profile.service';
import { ManagerService } from '../../services/manager.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

  /**
   * Группа формы профиля
   */
  public ProfileForm: FormGroup;

  /**
   * Группа формы
   */
  public AddManagerForm: FormGroup;

  /**
   * индикатор ошибки при отправке формы
   */
  public fromError: boolean = false;

  /**
   * индикатор загрузки
   */
  public isLoading: boolean = false;

  /**
   * индикатор загрузки для кнопки
   */
  public isLoadingSubmit: boolean = false;

  /**
   * список ошибок
   */
  public errors: string[];

  /**
   * список ошибок при сохранении менеджера
   */
  public errorsManager: string[];
  /**
   * Основная ошибка от сервера
   */
  public errorMessageManager: string = '';

  /**
   * список менеджеров
   */
  public managers: any;

  /**
   * стaртовая страница
   */
  public page: number = 1

  /**
   * данные аккаунта
   */
  public user: any;

  /**
   * Форм билдер
   */
  public fb: FormBuilder;

  public access: boolean = false;
  public modalObj: NgbModalRef;
  public errorsModal: any = []
  public modalLoading: boolean = false;

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    public profileService: ProfileService,
    public managerService: ManagerService,
    private router: Router,
    private modalService: NgbModal,
    private _ngZone: NgZone
  ) {
    this.fb = fb;
    this.ProfileForm = fb.group({
      name: [null, [Validators.required]],
      company: [null],
      // email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]],
    })

    this.AddManagerForm = this.addManagerFormFN(fb);
  }

  ngOnInit() {
    this.isLoading = true;

    // получение данных по профилю
    this.profileLoad();

    // получение данных по менеджерам
    this.pageLoad(this.page)
  }

  /**
   * Загрузка профиля
   */
  profileLoad() {
    this
      .profileService
      .getProfile()
      .then(success => {
        this.isLoading = false;
        this.access = true;
        this.ProfileForm.setValue({
          name: success.name,
          // email:   success.email,
          company: success.company,
          password: null,
        });
        this.user = success;
      }).catch(e => {
        this.isLoading = false;
        if (e.status == 401) {
          this.access = false;
        }
      })
  }

  /**
   * Загрузка страницы
   */
  pageLoad(page) {
    this
      .managerService
      .getManagers(page)
      .then((success) => {
        this.managers = success;
        // console.log('this.managers', this.managers)
      }).catch(error => {
        var dataError = error.json();
        // this.profileService.notificationsService.error(
        //   dataError.message,
        //   'managerService.getManagers'
        // );
      })
  }

  /**
   * Сохранение информации в профиле
   * @param value данные из формы
   */
  submit(value) {
    this.isLoadingSubmit = true;
    if (value.password == '') {
      value.password = null;
    }
    this
      .profileService
      .updateProfile(value)
      .then(success => {
        this.isLoadingSubmit = false;
        this.profileService.notificationsService.success(
          'Данные обновлены',
          'Нажмите, чтобы закрыть.'
        )
        this.profileLoad();

      })
      .catch(err => {
        this.isLoadingSubmit = false;
        var error = err.json();
        this.profileService.notificationsService.alert(
          'Ошибка при обновлении',
          error.message
        )

        // console.log('errProfile', err.json().message);

      })
  }


  /**
   * Удаление объекта
   * @param email индификатор пользователя
   */
  public delete(email: string) {
    if (confirm('Удалить пользователя ' + email + ' ?')) {
      this
        .managerService
        .deleteManager(email)
        .then(success => {
          this.profileService.notificationsService.success(
            'Менеджер удален',
            'Нажмите, чтобы закрыть.'
          )
          this.pageLoad(this.page)
        })
        .catch(e => {

        })
    }
  }

  /**
   * Отправка данных по сохраненеию менеджера
   * @param value 
   */
  public addManager(value, close) {
    this.modalLoading = true;
    this.errorsModal = [];
    this
      .managerService
      .addManager(value.email, value.name, value.password)
      .then(success => {
        this.modalLoading = false;
        this.profileService.notificationsService.success(
          'Менеджер добавлен',
          'Нажмите, чтобы закрыть.'
        )
        this.pageLoad(this.page)
        close();
      })
      .catch(e => {
        this.modalLoading = false;
        var ee = e.json();
        for (var err in ee.errors) {
          var el: any = ee.errors[err]
          el.forEach(element => {
            this.errorsModal.push(element)
          });
        }
      })
  }

  /**
   * Открытие модального окна добавление менеджера
   * @param content шаблон
   */
  openModal(content) {
    // console.log('content', content);
    this.AddManagerForm = this.addManagerFormFN(this.fb);
    this.modalObj = this.modalService.open(ModalComponent)
    this.modalObj.componentInstance.tpl = content;
    this.modalObj.componentInstance.save = this
    this.modalObj.componentInstance.close = this.modalObj.close;
  }

  /**
   * Создание формы
   * @param fb Билдер формы
   */
  addManagerFormFN(fb) {
    return fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]],
    })
  }
}