import { Component, OnInit, ViewChild } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { ScenarioService } from '../../services/scenario.service';
import { QuestionService } from '../../services/question.service';
import { TreeNode, TreeModel, TreeComponent } from 'angular-tree-component';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';



@Component({
  selector: 'app-workplace',
  templateUrl: './workplace.component.html',
  styleUrls: ['./workplace.component.css'],
  host: {'(window:scroll)': 'onScroll($event)'},
})
export class WorkplaceComponent implements OnInit {

  /**
   * данные по сценариям
   */
  public scenarios: any;

  /**
   * список групп
   */
  public groups: any;

  /**
   * список вопросов
   */
  public questions: any;

  /**
   * Текущий сценарий
   */
  public scenarioSelect: any;

  /**
   * текущая группа
   */
  public groupSelect: any;

  /**
   * текущий вопрос
   */
  public questionSelect: any;

  /**
   * получем модель для работы с деревом
   */
  @ViewChild('tree') treeComponent: TreeComponent;

  public toogleGroups: any = {};
  public toogleState = 'open';

  public isLoadScenario: boolean = true;
  public isLoadGroups: boolean = true;
  public isLoadQuestion: boolean = true;

  public groupsCache: any = {}

  public classScroll: string = 'st'

  constructor(
    public questionService: QuestionService,
    public scenarioService: ScenarioService,
    public groupService: GroupService,
    private _scrollToService: ScrollToService
  ) { }

  ngOnInit() {
    this.isLoadScenario = true;
    this
      .scenarioService
      .getScenarios()
      .then(success => {
        // if(success.data[0] != undefined){
        //   this.selectScenario(success.data[0]);
        // }

        this.scenarios = success;
        this.isLoadScenario = false;
        this.selectScenario({ panelId: 0 })
      })
      .catch(err => {
        this.isLoadScenario = false;
      })
  }


  /**
   * Выбор сценария 
   * @param scenario данные о сценарии 
   */
  selectScenario($event: any) {
    console.log('$event', $event);
    
    // this.questions = undefined;
    this.questionSelect = undefined;

    this.scenarioSelect = this.scenarios.data[$event.panelId];
    this.loadGroups(this.scenarioSelect.id);
  }

  /**
   * Полученеи данных группы по ID сценария
   * @param id ID сценария
   */
  loadGroups(scenario_id) {
    if (this.groupsCache[scenario_id] != undefined) {
      this.selectGroups(this.groupsCache[scenario_id])
    } else {
      this.isLoadGroups = true;
      this
        .groupService
        .getScenarioData(scenario_id)
        .then(success => {
          this.isLoadGroups = false;
          this.selectGroups(success.groups)
          this.groupsCache[scenario_id] = success.groups;     
        }).catch(err => {
          this.isLoadGroups = false;
        })
    }
  }

  /**
   * Выбираем группу и назанчем активной
   * @param groups группа
   */
  selectGroups(groups) {
    this.groups = groups;
    setTimeout(() => {
      if (this.treeComponent != undefined) {
        var root = this.treeComponent.treeModel.getFirstRoot()
        if (root != undefined) {
          root.expand()
          root.setIsActive(true)
        }
      }
    }, 50)
  }

  onActiveGroup($event) {
    // открываем дочернии
    $event.node.expand() 
    this.loadQuestion($event.node.data.id)
  }

  /**
   * Загружаем информацию о группе
   * @param group_id ID группы
   */
  loadQuestion(group_id) {
    this.questions = undefined;
    this.questionSelect = undefined;

    this.isLoadQuestion = true;
    this
      .questionService
      .getQuestions(group_id)
      .then(success => {

        this.isLoadQuestion = false;
        if (success.length > 0) {
          this.questionSelect = success[0];
        }
        this.questions = success;

        // console.log('this.questions', this.questions)
        this.toogleGroups = {}
        this.questions.forEach(element => {
          // console.log('element', element);
          this.toogleGroups[element.id] = true;  
        });
        console.log('s2', this.questions, this.toogleGroups);

      })
      .catch(err => {
        this.isLoadQuestion = false;
      })
  }

  ngAfterInit() {
    const treeModel: TreeModel = this.treeComponent.treeModel;
    const firstNode: TreeNode = treeModel.getFirstRoot();
    firstNode.setActiveAndVisible();
    firstNode.expand()
    firstNode.setIsActive(true);
  }


  /**
   * Получение путей для заголовка
   * @param tree модель дерева
   */
  getParentNames(tree: TreeModel): string {
    if(tree == undefined) return '';
    if(tree.getActiveNode() == undefined) return '';
    var res = [];
    if(tree.getActiveNode().path != undefined){
      tree.getActiveNode().path.forEach( v => {
        res.push(tree.getNodeById(v).data.name)
      })
    }
    return res.join(' - ');
  }

  toogleQ(id) {
    if(this.toogleGroups[id] != undefined){
      this.toogleGroups[id] = !this.toogleGroups[id];
    }
    // проверка сколько скрыто и сколько открыто
    this.isOpneClose()
  }

  sel(question){
    this.questionSelect = question;
    if(this.toogleGroups[question.id] != undefined){
      this.toogleGroups[question.id] = true;
      const config: ScrollToConfigOptions = {
        target: 'question' + question.id,
        offset: 120
      };
   
      this._scrollToService.scrollTo(config);
      // проверка сколько скрыто и сколько открыто
      this.isOpneClose()
    }
  }

  isOpneClose(){
    var open = 0;
    var close = 0;
    var all = 0;
    
    for(let e in this.toogleGroups){
      all++;
      if(this.toogleGroups[e]){
        open++;
      } else {
        close++;
      }
    }

    console.log(all, open, close)

    if(all == close){
      this.closeAll()
    } else if(all == open){
      this.openAll()
    }
  }

  closeAll(){
    this.toogleState = 'close'
    for(let e in this.toogleGroups){
      this.toogleGroups[e] = false;
    }

  }

  openAll(){
    this.toogleState = 'open'
    for(let e in this.toogleGroups){
      this.toogleGroups[e] = true;
    }
  }

  onScroll($event){
    // console.log('$event', $event.path[0].scrollTop);
    if($event.path[1].scrollY > 120){
      this.classScroll = 'fx col-md-3'
    } else {
      this.classScroll = 'st'
    }
  }
}
