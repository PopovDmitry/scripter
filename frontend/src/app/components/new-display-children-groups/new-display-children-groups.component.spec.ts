import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDisplayChildrenGroupsComponent } from './new-display-children-groups.component';

describe('NewDisplayChildrenGroupsComponent', () => {
  let component: NewDisplayChildrenGroupsComponent;
  let fixture: ComponentFixture<NewDisplayChildrenGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDisplayChildrenGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDisplayChildrenGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
