import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { QuestionService } from '../../services/question.service';

@Component({
  selector: 'app-new-display-children-groups',
  templateUrl: './new-display-children-groups.component.html',
  styleUrls: ['./new-display-children-groups.component.css']
})
export class NewDisplayChildrenGroupsComponent implements OnInit, OnDestroy, AfterViewInit {

  // получаем группы из вне
  @Input('group') group: any;
  @Input('pos') pos;
  @Input('scroll') scroll: string = "";

  @Output() onChange = new EventEmitter;

  public data: any[];
  public isLoadQuestion: boolean = false;

  public selectedQuestionId: number = 0;

  constructor(
    public questionService: QuestionService,
  ) { }

  ngOnInit() {
    // загрузка вопросов текущей группы
    this.isLoadQuestion = true;
    this
      .questionService
      .getAllQuestionsByGroupId(this.group.id)
      .then(data => {
        this.isLoadQuestion = false;
        this.data = data;
        this.onChange.emit({
          action: 'load',
          data: data
        })

        setTimeout(() => {
          var fx = document.getElementById('zooCol')
          fx.style.width = document.getElementById('zooStyle').offsetWidth - 30 + 'px';
          // document.getElementById('zooGroups').style.height = window.innerHeight - 150 + 'px'
        }, 50)

      })
      .catch(e => {
        this.isLoadQuestion = false;
      })
  }

  goTo(id) {
    this.onChange.emit({
      action: 'goTo',
      id: id
    })
    this.selectedQuestionId = id;
  }

  open(index) {
    if (this.data != undefined) {
      this.data[index].display = !this.data[index].display;
    }
  }

  counter() {
    var all = 0;
    var open = 0;
    var close = 0;
    if (this.data != undefined) {
      this.data.forEach(group => {
        all++;
        if (group.display) {
          open++;
        } else {
          close++;
        }
      })
    }


    return (all == close) ? 'close' : 'open';
  }

  closeAll() {
    if (this.data != undefined) {
      this.data.forEach(group => {
        group.display = false;
      })
    }

    this.onChange.emit({
      action: 'closeAll'
    })
  }

  openAll() {
    if (this.data != undefined) {
      this.data.forEach(group => {
        group.display = true;
      })
    }

    this.onChange.emit({
      action: 'openAll'
    })
  }

  ngAfterViewInit() {
  }

  inter(num: any): number {
    return parseInt(num)
  }

  ngOnDestroy() {
    // console.log('destroy')
  }

}
