import { Component, OnInit } from '@angular/core';
import { ScenarioService } from '../../services/scenario.service';

@Component({
  selector: 'app-scenario',
  templateUrl: './scenario.component.html',
  styleUrls: ['./scenario.component.css']
})
export class ScenarioComponent implements OnInit {

  /**
   * список менеджеров
   */
  public scenarios: any;
  
  /**
   * стaртовая страница
   */
  public page: number = 1

  /**
   * индикатор загрузки
   */
  public isLoading: boolean = false;

  constructor(
    private scenarioService: ScenarioService
  ) { }

  ngOnInit() {
    // получение данных по менеджерам
    this.pageLoad(this.page)
  }

  /**
   * Загрузка страницы
   */
  pageLoad(page){
    this
    .scenarioService
    .getScenarios(page)
    .then( (success) => {
      this.isLoading = true;
      this.scenarios = success;
      // console.log('this.managers', this.managers)
    }).catch( error => {
      var dataError = error.json();
      this.scenarioService.notificationsService.error(
        dataError.message,
        'scenarioService.getScenarios'
      );
    })
  }

  /**
   * Копирование сценария
   * @param id ID сценария
   */
  copy(id: number){
    this
      .scenarioService
      .copyScenario(id)
      .then( success => {
        this.scenarioService.notificationsService.success(
          'Копирование сценария успешно',
          'Нажмите, чтобы закрыть.'
        );
        this.pageLoad(this.page)
      })
      .catch( error => {
        var dataError = error.json();
        this.scenarioService.notificationsService.error(
          dataError.message,
          'scenarioService.copyScenario'
        );
      })
  }

  delete(id: number){
    if(confirm('Удалить сценарий?')){
      this
        .scenarioService
        .deleteScenario(id)
        .then( success => {
          this.scenarioService.notificationsService.success(
            'Сценарий удален',
            'Нажмите, чтобы закрыть.'
          );
          this.pageLoad(this.page)
        })
        .catch( error => {
          var dataError = error.json();
          this.scenarioService.notificationsService.error(
            dataError.message,
            'scenarioService.copyScenario'
          );
        })
    }
  }

}
