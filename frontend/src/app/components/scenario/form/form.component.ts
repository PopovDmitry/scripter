import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScenarioService } from '../../../services/scenario.service';
import { GroupService } from '../../../services/group.service';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {

  public isEdit: boolean = false;

  /**
   * хранение данных для подписки
   */
  public dataSub;

  /**
   * хранение данных параметров для подписки
   */
  public paramsSub;

  /**
   * Группа формы сценария
   */
  public ScenarioForm: FormGroup;

  /**
   * индикатор ошибки при отправке формы
   */
  public fromError: boolean = false;

  /**
   * индикатор загрузки
   */
  public isLoading: boolean = false;

  /**
   * Форм билдер
   */
  public fb: FormBuilder;

  public scenarioData: any;

  public submitLoading: boolean = false;

  public currentID: number;
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private scenarioService: ScenarioService,
    private groupService: GroupService,
    @Inject(FormBuilder) fb: FormBuilder,
  ) {
    this.ScenarioForm = fb.group({
      name: [null, [Validators.required]]
    })
   }

  ngOnInit() {
    
    // console.log(this.route.data);
    this.dataSub = this.route.data.subscribe( v => {
      this.isEdit = v.isEdit;
      if(v.isEdit){ // редактирование
        this.paramsSub = this.route.params.subscribe(v => {
          // console.log('edit', v);
          this.currentID = v.id
          this.getData(v.id)
        })
      } else { // добавление 
        this.isLoading = true;
        // console.log('add');
      }
    })
  }

  getData(id){
    this
    .groupService
    .getScenarioData(id)
    .then( success => {
      this.scenarioData = success
      // console.log(this.scenarioData);
      this.isLoading = true;
      this.ScenarioForm.setValue({
        name: this.scenarioData.name
      })
    })
  }


  refreshScenario(event){
    if(this.currentID != undefined){
      this.getData(this.currentID)
      // console.log('refreshScenario', this.currentID);
    }
  }

  relation($event){
    // console.log('$event', $event);
    if($event.action == 'add'){
      // добавялем связь
      this
        .groupService
        .addRealtion($event.id, this.scenarioData.id)
        .then( success => {
          this.groupService.notificationsService.success(
            'Группа добавлена к сценарию',
            'Нажмите, чтобы закрыть.'
          )
          this.getData(this.scenarioData.id)
        }).catch (err  => {
          var error = err.json();
          this.groupService.notificationsService.alert(
            'Ошибка при свзяке групп',
            error.message
          )
        })
    } else {
      this
      .groupService
      .deleteRealtion($event.id, this.scenarioData.id)
      .then( success => {
        this.groupService.notificationsService.success(
          'Группа удалена из сценария',
          'Нажмите, чтобы закрыть.'
        )
        this.getData(this.scenarioData.id)
      }).catch (err  => {
        var error = err.json();
        this.groupService.notificationsService.alert(
          'Ошибка при свзяке групп',
          error.message
        )
      })
    }
    
  }

  submit(v){
    this.submitLoading = true;
    if(!this.isEdit){ // создание сценария
      this
        .scenarioService
        .createScenario(v.name)
        .then( success => {
          // console.log(success.scenario_id);
          
          this.groupService.notificationsService.success(
            'Сценарий создан',
            'Нажмите, чтобы закрыть.'
          )
          this.router.navigate(['scenario/edit/' + success.scenario_id]);
          this.submitLoading = false;
        })
        .catch( err => {
          this.submitLoading = false;
          var error = err.json();
          this.groupService.notificationsService.alert(
            'Ошибка при создании сценария',
            error.message
          )
        })
    } else {
      this
        .scenarioService
        .updateScenario(v.name, this.scenarioData.id)
        .then( success => {
          this.groupService.notificationsService.success(
            'Сценарий обновлен',
            'Нажмите, чтобы закрыть.'
          )
          this.submitLoading = false;
        })
        .catch( err => {
          this.submitLoading = false;
          var error = err.json();
          this.groupService.notificationsService.alert(
            'Ошибка при обновлении сценария',
            error.message
          )
          this.submitLoading = false;
        })
    }
    
  }


  ngOnDestroy(){
    this.dataSub.unsubscribe();
    if(this.isEdit){
      this.paramsSub.unsubscribe();
    }

    this.scenarioData = undefined;
  }

  

}
