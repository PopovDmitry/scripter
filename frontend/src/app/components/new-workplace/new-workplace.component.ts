import { Component, OnInit, EventEmitter, NgZone } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { ScenarioService } from '../../services/scenario.service';
import { QuestionService } from '../../services/question.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { retry } from 'rxjs/operator/retry';
declare var $:any;
@Component({
  selector: 'app-new-workplace',
  templateUrl: './new-workplace.component.html',
  styleUrls: ['./new-workplace.component.css'],
  host: { '(window:scroll)': 'onScroll($event)' },
})

export class NewWorkplaceComponent implements OnInit {

  /**
   * данные по сценариям
   */
  public scenarios: any;

  /**
   * список групп
   */
  public groups: any;

  /**
   * список вопросов
   */
  public questions: any;

  /**
   * Текущий сценарий
   */
  public scenarioSelect: any;

  /**
   * текущая группа
   */
  public groupSelect: any;

  /**
   * текущий вопрос
   */
  public questionSelect: any;


  public toogleGroups: any = {};
  public toogleState = 'open';

  public isLoadScenario: boolean = true;
  public isLoadGroups: boolean = true;
  public isLoadQuestion: boolean = true;

  public groupsCache: any = {}

  public classScroll: string = 'st'


  constructor(
    public questionService: QuestionService,
    public scenarioService: ScenarioService,
    public groupService: GroupService,
    private _scrollToService: ScrollToService,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    this.isLoadScenario = true;
    this
      .scenarioService
      .getScenarios()
      .then(success => {
        // if(success.data[0] != undefined){
        //   this.selectScenario(success.data[0]);
        // }

        this.scenarios = success;
        this.isLoadScenario = false;
        this.selectScenario({ panelId: 0 })
      })
      .catch(err => {
        this.isLoadScenario = false;
      })
  }

  /**
   * Выбор сценария 
   * @param scenario данные о сценарии 
   */
  selectScenario($event: any) {
    this.questionSelect = undefined;
    this.groupSelect = undefined;
    this.questions = undefined;

    

    this.scenarioSelect = this.scenarios.data[$event.panelId];

    // console.log('this.scenarioSelect', this.scenarioSelect, $event.panelId)

    this.loadGroups(this.scenarioSelect.id);
  }

  /**
   * Полученеи данных группы по ID сценария
   * @param id ID сценария
   */
  loadGroups(scenario_id) {
    if (this.groupsCache[scenario_id] != undefined) {
      this.selectGroups(this.groupsCache[scenario_id])
    } else {
      this.isLoadGroups = true;
      this
        .groupService
        .getScenarioData(scenario_id)
        .then(success => {
          this.isLoadGroups = false;
          this.selectGroups(success.groups)
          // this.groupsCache[scenario_id] = success.groups;
        }).catch(err => {
          this.isLoadGroups = false;
        })
    }
  }

  /**
   * Выбираем группу и назанчем активной
   * @param groups группа
   */
  selectGroups(groups) {
    this.groupSelect = undefined;
    this.questions = undefined;

    this.groups = groups;
    // выбираем самую первую группу
    if (groups.length > 0) {
      this.groupSelect = groups[0];
    }
  }

  /**
   * Загружаем информацию о группе
   * @param group Объект группы
   */
  loadQuestion(group) {
    this.groupSelect = undefined;
    this.questions = undefined;
    setTimeout(() => {
      this.groupSelect = group;
    }, 50)
  }

  // скроллер 
  onScroll($event) {
    if($event.path[1].scrollY > 120){
      this.classScroll = 'fx'

      setTimeout( () =>{
        // if(document.getElementsByClassName('fx')){

        // }
        // var fx = document.getElementsByClassName('fx') as any;
        // if(fx.length > 0 ){
        //   fx[0].style.width = document.getElementById('zooStyle').offsetWidth - 30 + 'px';
        //   console.log()
        // }
        
      })

      
    } else {
      this.classScroll = 'st'
    }
  }

  /**
   * Отлов события из дочернего компонента
   * @param event событие
   */
  dataFromCom(event) {
    if (event.action == 'load') {
      this.questions = event.data;
    } else if (event.action == 'goTo') {
      const config: ScrollToConfigOptions = {
        target: 'question' + event.id,
        offset: 120
      };

      this._scrollToService.scrollTo(config);

      // авто открытие плашки
      if (this.questions != undefined) {
        this.questions.forEach(group => {
          if (group.questions.length > 0) {
            group.questions.forEach(q => {
              if(q.id == event.id){
                q.display = true;
              }
            })
          }
        });
      }

    } else if (event.action == 'openAll') {
      this.setOpneClosePOsition(true)
    } else if (event.action == 'closeAll') {
      this.setOpneClosePOsition(false)
    }
  }

  isOpenClose() {
    var open = 0;
    var close = 0;
    var all = 0;
    if (this.questions != undefined) {
      this.questions.forEach(group => {
        if (group.questions.length > 0) {
          group.questions.forEach(q => {
            all++;
            if (q.display) {
              open++;
            } else {
              close++;
            }
          })
        }
      });
    }

    return (all == close) ? 'close' : 'open';

    // return {
    //   open,
    //   close,
    //   all
    // }
  }

  setOpneClosePOsition(isOpen) {
    if (this.questions != undefined) {
      this.questions.forEach(group => {
        if (group.questions.length > 0) {
          group.questions.forEach(q => {
            q.display = isOpen;
          })
        }
      });
    }
  }

  

}
