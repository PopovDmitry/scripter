import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWorkplaceComponent } from './new-workplace.component';

describe('NewWorkplaceComponent', () => {
  let component: NewWorkplaceComponent;
  let fixture: ComponentFixture<NewWorkplaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewWorkplaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewWorkplaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
