import { Component, OnInit, ViewChild, Input, Inject, Output, EventEmitter, OnChanges } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { TreeNode, TreeModel, TreeComponent } from 'angular-tree-component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit, OnChanges {

  /**
   * выбранный элелмент
   */
  public selected: any;

  /**
   * Индикатор состояния
   */
  public state: string = 'none'

  /**
   * получем модель для работы с деревом
   */
  @ViewChild('tree') treeComponent: TreeComponent;

  /**
   * получем модель для работы с деревом
   */
  @Input('treeGroups') treeGroups: any;

  /**
   * заголовок
   */
  @Input('titler') titler: string = "Группы";

  /**
   * индикатор включения возможности добавлять группу в корень
   */
  @Input('addRoot') addRoot: boolean = true;

  /**
   * Индикатор уже существущих ID групп
   */
  @Input('ids') ids: number[] = [];


  // @Input('noids') noids: number[] = [];


  /**
   * Индикатор Использования в сценария
   */
  @Input('isScenario') isScenario: boolean = true;

  /**
   * тип связи
   */
  @Input('typeRelation') typeRelation: string;

  /**
   * сообщаем наверх, о действии с привязкой
   */
  @Output() relation = new EventEmitter();

  /** событие обновления сценария */
  @Output() refreshScenario = new EventEmitter();


  /**
   * индикатор загрузки
   */
  public isLoading: boolean = false;

  /**
   * Форма
   */
  public GroupForm: FormGroup;

  public fb: FormBuilder;

  public modalObj: NgbModalRef;

  public selectedTreeList: any[] = []

  constructor(
    private groupService: GroupService,
    @Inject(FormBuilder) fb: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.fb = fb;
  }

  ngOnInit() {
    if (this.treeGroups != undefined) {
      // this.treeGroups = this.treeGroups;
    } else {
      this.loadGroups();
    }


    // console.log('this.data', this.data);

  }

  ngOnChanges(changes) {

    if (changes.ids != undefined) {
      this.loadGroups();
    }

  }

  loadGroups() {
    // console.log('loadGroups', this.ids)
    this
      .groupService
      .getAllGroups()
      .then(success => {
        // console.log('groups', success);
        this.treeGroups = success;
        this.clearIds();
        if (this.treeComponent != undefined) {
          // console.log('tree update')
          if (this.treeComponent.treeModel != undefined) {
            // console.log('tree update')
            this.treeComponent.treeModel.update();
          }
        }

        // 
        // this.
      })
      .catch(e => {
        console.log('groupService.getAllGroups.err', e);
      })
  }

  onActive($event) {
    // console.log('activate', $event);
    this.state = 'options';
    this.selected = $event.node.data;
  }

  /**
   * исключение id
   */
  clearIds() {
    if (this.ids.length > 0) {
      this.ids.forEach(e => {
        // console.log('this.treeGroups', this.treeGroups);

        this.treeGroups = this.treeGroups.filter(t => {
          return (t.id == e) ? false : true;
        })

        // console.log('this.treeGroups2', this.treeGroups);
      })
    }
  }

  onDeactivate($event) {
    this.state = 'none';
    this.selected = undefined;
  }

  close(tree) {
    this.state = 'none';
    tree.treeModel.getFocusedNode().toggleActivated()
    this.selected = undefined;
  }

  ngAfterInit() {
    const treeModel: TreeModel = this.treeComponent.treeModel;
    const firstNode: TreeNode = treeModel.getFirstRoot();
    firstNode.setActiveAndVisible();
  }

  /**
   * отображение шаблона добавления
   * @param tree 
   */
  addTemp(tree: TreeComponent, parent_id?) {
    // console.log('tree', tree.treeModel.getFocusedNode());

    // this.close(tree);
    if (parent_id != undefined && tree.treeModel.getFocusedNode() != null) {
      tree.treeModel.getFocusedNode().setIsActive(false)
    }
    this.state = 'add';
    this.GroupForm = this.fb.group({
      name: [null, [Validators.required]],
      parent_id: [((parent_id != undefined)) ? 0 : tree.treeModel.getFocusedNode().data.id]
    })
  }

  /**
   * Отображение формы
   * @param node 
   */
  editTmp(node) {
    this.state = 'edit';
    this.selectedTreeList = this.getList(node.id, node.parent_id);
    this.GroupForm = this.fb.group({
      name: [null, [Validators.required]],
      parent_id: [null],
      sort: [null, [Validators.required]]
    })
    this.GroupForm.setValue({
      name: node.name,
      parent_id: node.parent_id,
      sort: node.sort
    })



  }

  editSubmit(tree, value) {




    this.isLoading = true;
    var fNode = tree.treeModel.getFocusedNode();

    this
      .groupService
      .updateGroup(this.selected.id, value.name, value.parent_id, value.sort)
      .then(success => {
        this.isLoading = false;
        this.groupService.notificationsService.success(
          'Группа обновлена',
          'Нажмите, чтобы закрыть.'
        )
        this.state = 'none';

        this.GroupForm = undefined;
        // if(!this.isScenario && this.typeRelation != 'delete'){
        // если обновление происходит в привязанных
        if (this.isScenario && this.typeRelation == 'delete') {
          this.refreshScenario.emit({
            action: 'update'
          })

        } else {
          this.loadGroups();
        }

        // }


      })
      .catch(err => {
        this.isLoading = false;
        var error = err.json();
        this.groupService.notificationsService.alert(
          'Ошибка при обновлении',
          error.message
        )
      })
  }

  /**
   * отправка данных
   * @param tree 
   */
  addSubmit(tree, value) {
    this.isLoading = true;
    this
      .groupService
      .createGroup(value.name, value.parent_id)
      .then(success => {
        this.isLoading = false;
        this.groupService.notificationsService.success(
          'Группа добавлена',
          'Нажмите, чтобы закрыть.'
        )
        this.state = 'none';

        this.GroupForm = undefined;
        // if(this.typeRelation != undefined || this.typeRelation != 'delete'){
        if (this.isScenario && this.typeRelation == 'delete') {
          this.refreshScenario.emit({
            action: 'update'
          })

        } else {
          this.loadGroups();
        }
        // }

      })
      .catch(err => {
        this.isLoading = false;
        var error = err.json();
        this.groupService.notificationsService.alert(
          'Ошибка при создании',
          error.message
        )
      })
  }



  /**
   * Клонирование группы
   * @param id 
   */
  clone(id) {
    this.isLoading = true;
    this
      .groupService
      .cloneGroup(id)
      .then(success => {
        this.isLoading = false;
        if (this.isScenario && this.typeRelation == 'delete') {
          this.refreshScenario.emit({
            action: 'update'
          })
        } else {
          this.loadGroups();
        }
        this.state = 'none';

        this.groupService.notificationsService.success(
          'Группа скопирована',
          'Нажмите, чтобы закрыть.'
        )
      })
      .catch(err => {
        this.isLoading = false;
        var error = err.json();
        this.groupService.notificationsService.alert(
          'Ошибка при копировании',
          error.message
        )
      })
  }

  /**
   * Копировние внутреннего элемента
   * @param id 
   */
  cloneChild(id) {
    this.isLoading = true;
    this
      .groupService
      .copyTreeById(id)
      .then(success => {
        this.isLoading = false;
        this.groupService.notificationsService.success(
          'Группа скопирована',
          'Нажмите, чтобы закрыть.'
        )
        if (this.isScenario && this.typeRelation == 'delete') {
          this.refreshScenario.emit({
            action: 'update'
          })

        } else {
          this.loadGroups();
        }
        this.state = 'none';
      })
      .catch(err => {
        this.isLoading = false;
        var error = err.json();
        this.groupService.notificationsService.alert(
          'Ошибка при копировании',
          error.message
        )
      })
  }

  /**
   * Удаление
   * @param id 
   */
  delete(id) {
    if (confirm('Удалить группу?')) {
      this.isLoading = true;
      this
        .groupService
        .deleteGroup(id)
        .then(success => {
          this.isLoading = false;
          this.groupService.notificationsService.success(
            'Группа удалена',
            'Нажмите, чтобы закрыть.'
          )
          if (this.isScenario && this.typeRelation == 'delete') {
            this.refreshScenario.emit({
              action: 'update'
            })

          } else {
            this.loadGroups();
          }
          this.state = 'none';
        })
        .catch(err => {
          this.isLoading = false;
          var error = err.json();
          this.groupService.notificationsService.alert(
            'Ошибка при удалении',
            error.message
          )
        })
    }
  }

  /**
   * создание связи
   * @param id 
   */

  addRelation(id, tree) {
    this.close(tree)
    this.treeGroups.forEach(element => {
      if (element.id == id) {
        element.name = element.name + ' (идет связка со сценарием...)'
      }
    });
    this.relation.emit({
      action: 'add',
      id: id
    })
  }

  /**
   * Удаление связи
   * @param id 
   */
  deleteRelation(id, tree) {
    this.close(tree)
    this.treeGroups.forEach(element => {
      if (element.id == id) {
        element.name = element.name + ' (идет разрыв связи со сценарием...)'
      }
    });
    this.relation.emit({
      action: 'delete',
      id: id
    })
  }

  openModal(content) {
    // console.log(content);
    this.modalObj = this.modalService.open(ModalComponent, { size: 'lg' })
    this.modalObj.componentInstance.tpl = content;
    this.modalObj.componentInstance.save = this
    this.modalObj.componentInstance.close = this.modalObj.close;
  }

  /**
   * получение списка деревеьев для смена предка
   * @param id 
   */
  getList(id: number, parent_id: number) {
    var deep = (data: any[], lavel: number = 1): any => {
      var res = [];
      if (data.length > 0) {
        data.forEach(row => {
          if (row.id != id) {
            res.push({
              name: row.name,
              lavel: lavel,
              id: row.id,
              selected: (row.id == parent_id) ? true : false
            });
            if (row.children != undefined) {
              var r = deep(row.children, lavel + 1);
              if (r.length > 0) {
                r.forEach(element => {
                  res.push({
                    name: element.name,
                    lavel: element.lavel,
                    id: element.id,
                    selected: element.selected
                  });
                });
              }
            }
          }
        })
      }

      return res;
    }

    var d = [{
      name: 'Корень',
      lavel: 0,
      id: 0,
      selected: (parent_id == 0) ? true : false
    }];

    if (this.treeGroups.length > 0) {
      var f = deep(this.treeGroups);
      if (f.length > 0) {
        f.forEach(element => {
          d.push(element)
        });
      }
    }
    return d;
  }

}
