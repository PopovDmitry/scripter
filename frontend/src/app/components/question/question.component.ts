import { Component, OnInit, Input, Inject, OnDestroy } from '@angular/core';
import { QuestionService } from '../../services/question.service';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  @Input('group') group: any;

  @Input('close') close: any;

  @Input('save') save: any;

  public questions: any;

  public state: string = 'list';

  public QuestionForm: FormGroup;

  public fb: FormBuilder;

  public answers;

  public isLoadingSubmit: boolean = false;

  public currentId: number;

  

  /**
   * индикатор загрузки
   */
  public isLoading: boolean = false;

  public editor: any = {};
  public editorContent = `<h3>I am Example content</h3>`;
  public editorOptions = {
    placeholder: "insert content..."
  };

  constructor(
    public questionService: QuestionService,
    @Inject(FormBuilder) fb: FormBuilder,
  ) {
    this.fb = fb;
  }

  ngOnInit() {
    this.loadQuestionsByGroup(this.group.id)
  }

  ngAfterViewInit() {
    
  }

  loadQuestionsByGroup(group_id: number) {
    this.isLoading = true;
    this
      .questionService
      .getQuestions(group_id)
      .then(success => {
        this.isLoading = false;
        this.questions = success;
      })
      .catch(err => {
        this.isLoading = false;
        var error = err.json();
        this.questionService.notificationsService.alert(
          'Ошибка при получение данных',
          error.message
        )
      })
  }

  createQuestion() {
    this.state = 'add';
    this.QuestionForm = this.fb.group({
      question: [null, [Validators.required]],
      answers: this.fb.array([]),
      sort: [null, [Validators.required]]
    })


    

    this
      .questionService
      .getMaxSort(this.group.id)
      .then( (success) => {
        this.QuestionForm = this.fb.group({
          question: [null, [Validators.required]],
          answers: this.fb.array([]),
          sort: [(success.sort + 1), [Validators.required]]
        })

        this.addAnswer();

        this.displayWys()

      } )
  }

  /**
   * Показать форму с заполнеными данными вопроса и ответов
   * @param id ID вопроса
   */
  updateQuestion(question){
    this.state = 'edit';
    this.QuestionForm = undefined;
    this.QuestionForm = this.fb.group({
      question: [question.question, [Validators.required]],
      answers: this.fb.array(question.answers),
      sort: [question.sort, [Validators.required]]
    })
    this.currentId = question.id
    this.displayWys();
  }

  addAnswer() {
    this.answers = this.QuestionForm.get('answers') as FormArray;
    this.answers.push(this.fb.control(''));
  }

  deleteAnswer(i) {
    if (confirm('Удалить ответ?')) {
      this.answers = this.QuestionForm.get('answers') as FormArray;
      this.answers.removeAt(i);
    }
  }

  addSubmit(value){
    this.isLoadingSubmit = true;
    this
      .questionService
      .createQuestion(this.group.id, value.question, value.answers, value.sort)
      .then ( succeess => {
        this.isLoadingSubmit = false;
        this.questionService.notificationsService.success(
          'Вопрос добавлен в группу',
          'Нажмите, чтобы закрыть.'
        )
        this.loadQuestionsByGroup(this.group.id);
        this.state = 'list';
        this.hideWis()
      })
      .catch(err => {
        var error = err.json();
        this.questionService.notificationsService.alert(
          'Ошибка создании вопроса',
          error.message
        )
        this.isLoadingSubmit = false;
      })
  }

  editSubmit(value){
    this.isLoadingSubmit = true;
    this
      .questionService
      .updateQuestion(this.currentId, value.question, value.answers, value.sort)
      .then ( succeess => {
        this.isLoadingSubmit = false;
        this.questionService.notificationsService.success(
          'Вопрос добавлен в группу',
          'Нажмите, чтобы закрыть.'
        )
        this.loadQuestionsByGroup(this.group.id);
        this.state = 'list';
        this.hideWis();
        this.currentId = undefined;
      })
      .catch(err => {
        var error = err.json();
        this.questionService.notificationsService.alert(
          'Ошибка создании вопроса',
          error.message
        )
        this.isLoadingSubmit = false;
      })
  }

  /**
   * смена сортировки
   * @param old_index стая позиция
   * @param new_index новая позиция
   */
  move(old_index, new_index) {
    this.answers = this.QuestionForm.get('answers') as FormArray;
    var newP = this.answers.at(new_index).value
    var oldP = this.answers.at(old_index).value

    this.answers.at(new_index).patchValue(oldP);
    this.answers.at(old_index).patchValue(newP);
  }

  closer() {
    this.close()
  }

  deleteQuestion(id: number) {
    if (confirm('Вы действительно хотите удалить вопрос?')) {
      this
        .questionService
        .deleteQuestion(id)
        .then(success => {
          this.questionService.notificationsService.success(
            'Вопрос удален из группы',
            'Нажмите, чтобы закрыть.'
          )
          this.loadQuestionsByGroup(this.group.id)
        })
        .catch(err => {
          this.isLoading = false;
          var error = err.json();
          this.questionService.notificationsService.alert(
            'Ошибка при получение данных',
            error.message
          )
        })
    }
  }

  copyQuestion(id: number){
    this
      .questionService
      .copyQuestion(id)
      .then( success => {
        console.log('copySuccees', success);
        this.questionService.notificationsService.success(
          'Вопрос скопирован',
          'Нажмите, чтобы закрыть.'
        )
        this.loadQuestionsByGroup(this.group.id)
      })
      .catch( (err) => {
        var error = err.json();
        this.questionService.notificationsService.alert(
          'Ошибка при получение данных',
          error.message
        )
      })
  }

  displayWys(){
    // console.log('si')
    // setTimeout(() => {
    //   tinymce.init({
    //     skin_url: 'assets/skins/lightgray',
    //     selector: '.editorWysiwyg',
    //     menubar: false,
    //     statusbar: false,
    //     style_formats: [
    //       {title: 'Black', inline: 'span', styles: { color: '#000000' }},
    //       {title: 'Red', inline: 'span', styles: { color: '#ff0000' }},
    //       {title: 'Green', inline: 'span', styles: { color: '#00ff00' }},
    //       {title: 'Blue', inline: 'span', styles: { color: '#0000ff' }},
    //       {title: 'Yellow', inline: 'span', styles: { color: '#ffff00' }},
    //       {title: 'Orange', inline: 'span', styles: { color: '#ffa500' }}
    //     ],
    //     setup: editor => {
    //       this.editor[editor.id] = editor;
          
    //       editor.on('change', () => {
    //         const content = editor.getContent();
    //         // console.log('content', content)
    //         var r = this.QuestionForm.get('answers') as any;
    //         var z = r.controls[parseInt(editor.id)] as FormControl;
    //         z.setValue(content)
    //       });
    //     },
    //   });
    // }, 200)
  }

  hideWis(){
    // console.log('this.editor', this.editor)
    // for(var i in this.editor){
    //   tinymce.remove(this.editor[i]);
    // }
    
  }

  backList(){
    this.state = 'list'
    this.hideWis()
  }

  ngOnDestroy() {
    this.hideWis()
    // tinymce.remove(this.editor);
  }

}