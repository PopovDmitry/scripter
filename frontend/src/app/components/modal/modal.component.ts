import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() save: any;
  @Input() close: any;
  @Input() tpl;
  @Input() errorsManager;

  constructor(private viewContainerRef: ViewContainerRef, public activeModal: NgbActiveModal) {}

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      var res = this.viewContainerRef.createEmbeddedView(this.tpl, {
        save: this.save,
        close: this.activeModal.close,
        errorsManager: this.errorsManager,
      });      
    })
    
  }

}
