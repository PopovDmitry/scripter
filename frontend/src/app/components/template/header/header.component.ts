import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLogin: boolean = false;

  constructor(
    public apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this
      .apiService
      .loggedIn$.asObservable()
      .subscribe( res => {
        this.isLogin = res;
      })
  }

  logout(){
    this.apiService.logout();
  }

}
