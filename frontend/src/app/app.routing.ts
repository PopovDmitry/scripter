import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuard } from './services/auth-guard.service';

import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormComponent } from './components/scenario/form/form.component';
import { WorkplaceComponent } from './components/workplace/workplace.component';
import { NewWorkplaceComponent } from './components/new-workplace/new-workplace.component';

const appRoutes: Routes =[
    {
      path: '',
      component: NewWorkplaceComponent,
      canActivate: [AuthGuard]
      // redirectTo: 'login',
      // pathMatch: 'full'
    },
    {
      path: 'old',
      component:  WorkplaceComponent,
      canActivate: [AuthGuard]
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'signup',
      component: SignupComponent
    },
    {
      path: 'profile',
      component: ProfileComponent,
      canActivate: [AuthGuard]
    },
    {
      path: 'settings',
      component: DashboardComponent,
      canActivate: [AuthGuard]
    },
    {
      path: 'scenario/add',
      component: FormComponent,
      canActivate: [AuthGuard],
      data: {isEdit: false}
    },
    {
      path: 'scenario/edit/:id',
      component: FormComponent,
      canActivate: [AuthGuard],
      data: {isEdit: true}
    }
  ]

export const routing = RouterModule.forRoot(appRoutes);
