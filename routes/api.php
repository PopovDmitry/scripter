<?php

use Illuminate\Http\Request;
use App\Mail\RegMail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// получение ключа доступа
// Route::get('/token', 'TokenController@token');

// авторизация
Route::post('login', 'TokenController@login');

// регистрация
Route::post('signup', 'TokenController@signup');

// Route::get('test', function () {
//     // $res = \Mail::to('pbdincom@gmail.com')->send(new RegMail(\App\User::find(1), 'dd'));
//     // return [$res];
//     // return new RegMail(\App\User::find(1), 'dd');
// });



Route::middleware('jwt.auth')->get('/', function () {
  return 'true';
});

Route::group(['middleware' => ['jwt.auth', 'permission']], function () {
    // тестовый метод
    Route::get('email', 'TokenController@email');

    // Оновление ключа доступа
    Route::get('refresh', 'TokenController@refresh');

    // выход из системы
    Route::get('logout', 'TokenController@logout');

    // обработка профайла
    Route::get('profile', 'ProfileController@profile'); // отдача данных по профайлу
    Route::put('profile', 'ProfileController@update'); // обновление профайл

    /****** МЕНЕДЖЕРЫ *****/
    Route::get('manager', 'ManagerController@index'); // отдача по менеджерам
    Route::post('manager', 'ManagerController@create'); // Создание менеджера
    Route::delete('manager/{email}', 'ManagerController@delete'); // удаление менеджера

    /****** СЦЕНАРИИ *****/
    Route::get('scenario', 'ScenarioController@index'); // отдача списка сценариев
    Route::get('scenario/view/{id}', 'ScenarioController@view'); // отдача одного сценария сценариев
    Route::post('scenario', 'ScenarioController@create'); // Создание сценария
    Route::put('scenario/{id}', 'ScenarioController@update'); // Редактирование сценария
    Route::delete('scenario/{id}', 'ScenarioController@delete'); // удаление сценария
    Route::get('scenario/copy/{id}', 'ScenarioController@copy'); // копирование менеджера

    /**** ГРУППЫ ****/
    Route::get('groups', 'GroupsController@index'); // отдача списка групп
    Route::get('groups/view/{id}', 'GroupsController@view'); // отдача информация о группе
    Route::get('groups/scenario/{id}', 'GroupsController@scenario'); // отдача групп по id сценарию
    Route::post('groups', 'GroupsController@create'); // созадние группы
    Route::put('groups/{id}', 'GroupsController@update'); // обновление группы
    Route::delete('groups/{id}', 'GroupsController@delete'); // удаление группы
    Route::get('groups/copyAsRoot/{id}', 'GroupsController@copyAsRoot'); // Копирование группы как корневого элемента
    Route::get('groups/copyTreeById/{id}', 'GroupsController@copyTreeById'); // Копирование группы как дочернего элемента
    Route::post('groups/relation', 'GroupsController@relation'); // связка сценария и группы
    Route::delete('groups/relation/{scenario_id}/{group_id}', 'GroupsController@relationDelete'); // отвязка сценария и группы

    /**** ВОПРСЫ и ОТВЕТЫ ****/
    Route::get('question/group/{group_id}', 'QuestionController@index'); // отдача списка ответов/вопросов
    Route::get('question/group/all/{group_id}', 'QuestionController@all'); // отдача списка всех ответов/вопросов по дочерним группам
    Route::get('question/view/{id}', 'QuestionController@view'); // отдача одного ответа/вопросов
    Route::post('question', 'QuestionController@create'); // Создание вопросов/ответов
    Route::put('question/{id}', 'QuestionController@update'); // Обновление вопросов/ответов
    Route::delete('question/{id}', 'QuestionController@delete'); // удаление вопросов/ответов
    Route::get('question/getMaxSort/{group_id}', 'QuestionController@getMaxSort'); // отдача одного ответа/вопросов
    Route::get('question/copyQuestion/{id}', 'QuestionController@copyQuestion');





    // получение информации о пользователе
    // Route::get('user', function (Request $request) {
    //     return $request->user()->only('name', 'email', 'company_id', 'is_actived');
    // });
});
