<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/app');
});


Route::group(
[
    // 'namespace'  => 'Backpack\Base\app\Http\Controllers',
    // 'middleware' => 'web',
    'prefix'     => config('backpack.base.route_prefix'),
],
function () {
    Route::auth();
    Route::get('dashboard', 'AdminController@dashboard');
    Route::get('/', 'AdminController@redirect');
    Route::get('logout', 'Auth\LoginController@logout');

    CRUD::resource('users', 'Admin\UsersCrudController');
    CRUD::resource('company', 'Admin\CompanyCrudController');
    CRUD::resource('roles', 'Admin\RoleCrudController');
    CRUD::resource('scenarios', 'Admin\ScenarioCrudController');
    // echo 'dd';
    // exit();
    // if not otherwise configured, setup the auth routes
    // if (config('backpack.base.setup_auth_routes')) {
    //     Route::auth();
    //     Route::get('logout', 'Auth\LoginController@logout');
    // }
    // if not otherwise configured, setup the dashboard routes
    // if (config('backpack.base.setup_dashboard_routes')) {

    // }
});

// /Route::get('/home', 'HomeController@index')->name('home');
