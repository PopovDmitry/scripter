<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScenariosGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scenario_group', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('scenario_id')->unsigned()->index()->foreign()->references("id")->on("scenarios")->onDelete("cascade");
          $table->integer('group_id')->unsigned()->index()->foreign()->references("id")->on("groups")->onDelete("cascade");
          // $table->integer('group_id')->unsigned()->index();
          $table->timestamps();

          // $table->foreign('group_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scenario_group');
    }
}
