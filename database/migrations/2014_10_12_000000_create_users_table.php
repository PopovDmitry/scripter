<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id'); // автоинкремент
            $table->string('name')->nullable(); // имя
            $table->string('email')->unique(); // емаил
            $table->string('password'); // пароль
            $table->integer('company_id'); // ID компании
            $table->boolean('is_actived')->default(true); // статус активации

            $table->rememberToken();
            $table->timestamps();

            $table->index(['email', 'company_id', 'is_actived']);
        });

        // создание первого аккаунта
        // $user = new User;
        // $user->name = 'Dmitry';
        // $user->email = 'pbd.incom@gmail.com';
        // $user->password = bcrypt('3104250');
        // $user->company_id = 1;
        // $user->is_actived = true;
        // $user->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
