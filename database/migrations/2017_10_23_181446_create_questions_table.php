<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned()->index()->foreign()->references("id")->on("groups")->onDelete("cascade");
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->text('question'); // вопросы
            $table->jsonb('answers'); // ответы
            $table->integer('sort')->unsigned();
            $table->timestamps();

            $table->index(['created_at', 'user_id', 'company_id', 'group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
