<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Scenario::class, function (Faker $faker) {
  $user = factory(App\User::class)->create();
  // dd($user);
  return [
      'name' => $faker->sentence($nbWords = 3, $variableNbWords = true) ,
      'user_id' => $user->id,
      'company_id' => $user->company_id
  ];
});
