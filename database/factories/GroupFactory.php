<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Group::class, function (Faker $faker) {
    $user = factory(App\User::class)->create();
    return [
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'user_id' => $user->id,
        'company_id' => $user->company_id,
        'is_root' => true,
        'parent_id' => 0,
        'sort' => 1,
    ];
});
