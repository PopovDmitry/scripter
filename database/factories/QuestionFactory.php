<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Question::class, function (Faker $faker) {

    $user = factory(App\User::class)->create();
    $group = factory(App\Models\Group::class)->create();
    return [
      'question' => $faker->sentence($nbWords = 3, $variableNbWords = true),
      'user_id' => $user->id,
      'company_id' => $user->company_id,
      'group_id' => $group->id,
      'answers' => json_encode([
        $faker->sentence($nbWords = 3, $variableNbWords = true),
        $faker->sentence($nbWords = 3, $variableNbWords = true),
        $faker->sentence($nbWords = 3, $variableNbWords = true),
        $faker->sentence($nbWords = 3, $variableNbWords = true),
        $faker->sentence($nbWords = 3, $variableNbWords = true),
        $faker->sentence($nbWords = 3, $variableNbWords = true),
        $faker->sentence($nbWords = 3, $variableNbWords = true),
        $faker->sentence($nbWords = 3, $variableNbWords = true),
      ]),
      'sort' => $faker->randomDigitNotNull
    ];
});
