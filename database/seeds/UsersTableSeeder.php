<?php

use Illuminate\Database\Seeder;
use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;

class UsersTableSeeder extends Seeder
{
    var $all;
    var $onlyView;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Админ
      DB::table('users')->insert([
          'name' => 'Dmitry',
          'email' => 'pbd.incom@gmail.com',
          'password' => bcrypt('3104250'),
          'company_id' => 1,
          'is_actived' => true
      ]);

      // Админ компании
      DB::table('users')->insert([
          'name' => 'TestACLCompanyAdmin',
          'email' => 'company.admin@scenario.com',
          'password' => bcrypt('1234567'),
          'company_id' => 1,
          'is_actived' => true
      ]);

      // Обычный менеджер 1
      DB::table('users')->insert([
          'name' => 'TestACLManager',
          'email' => 'company.manager@scenario.com',
          'password' => bcrypt('1234567'),
          'company_id' => 1,
          'is_actived' => true
      ]);

      // Обычный менеджер 2
      DB::table('users')->insert([
          'name' => 'TestACLManager2',
          'email' => 'company.manager2@scenario.com',
          'password' => bcrypt('1234567'),
          'company_id' => 1,
          'is_actived' => true
      ]);

      // $users = factory(\App\User::class, 50)->create(['company_id' => 1]);
      // foreach ($users as $key => $user) {
      //   $user->assignRole('user');
      // }

      // добавляем роль полного доступа
      $this->ACLAdmin();

      // добавляем роль админа компании
      $this->ACLAdminCompany();

      // создаем группу для обычного пользователя
      $this->ACLUser();

      $TestACLAdminCompany = \App\User::find(2);
      $TestACLAdminCompany->assignRole('company_admin');

      $TestACLManager = \App\User::find(3);
      $TestACLManager->assignRole('user');

      $TestACLManager = \App\User::find(4);
      $TestACLManager->assignRole('user');

      // $user = \App\User::first();
      // $user->assignRole('company_admin');
    }

    /**
    * Создание прав доступ для админа
    */
    protected function ACLAdmin() {
      // создаем РОЛЬ
      $roleAdmin = new Role();
      $roleAdmin->name = 'Администратор';
      $roleAdmin->slug = env('ROLE_ADMIN', 'administrator');
      $roleAdmin->description = 'Все привелегии';
      $roleAdmin->save();

      // назначаем админу профайл доступ

      $roleAdmin->addPermission('manager');
      $roleAdmin->addPermission('copy.manager', true);

      $roleAdmin->addPermission('scenario');
      $roleAdmin->addPermission('copy.scenario', true);

      $roleAdmin->addPermission('group');
      $roleAdmin->addPermission('copy.group', true);

      $roleAdmin->addPermission('answer');
      $roleAdmin->addPermission('copy.answer', true);

      $roleAdmin->addPermission('question');
      $roleAdmin->addPermission('copy.question', true);

      $roleAdmin->addPermission('company');

      $roleAdmin->addPermission('view.profile', true);
      $roleAdmin->addPermission('update.profile', true);

      $roleAdmin->addPermission('view.token');

      $user = \App\User::first();
      $user->assignRole($roleAdmin);




      // созадем права доступа

    }

    /**
    * Создание прав доступ для админа компании
    */
    protected function ACLAdminCompany() {
      // создаем РОЛЬ
      $roleAdminCompany = new Role();
      $roleAdminCompany->name = 'Администратор компании';
      $roleAdminCompany->slug  = env('ROLE_COMPANY_ADMIN', 'company_admin');
      $roleAdminCompany->description = 'Доступ к всем функциям компании';
      $roleAdminCompany->save();

      $roleAdminCompany->addPermission('scenario');
      $roleAdminCompany->addPermission('copy.scenario', true);

      $roleAdminCompany->addPermission('manager');
      $roleAdminCompany->addPermission('copy.manager', true);

      $roleAdminCompany->addPermission('group');
      $roleAdminCompany->addPermission('copy.group', true);

      $roleAdminCompany->addPermission('answer');
      $roleAdminCompany->addPermission('copy.answer', true);

      $roleAdminCompany->addPermission('question');
      $roleAdminCompany->addPermission('copy.question', true);

      $roleAdminCompany->addPermission('company');

      $roleAdminCompany->addPermission('view.profile', true);
      $roleAdminCompany->addPermission('update.profile', true);
      $roleAdminCompany->addPermission('view.token');
    }



    /**
    * Создание прав доступ для пользователя
    */
    protected function ACLUser(){
      // создаем РОЛЬ
      $roleUser = new Role();
      $roleUser->name = 'Пользователь';
      $roleUser->slug = env('ROLE_user', 'user');
      $roleUser->description = 'Чтение сценариев компании';
      $roleUser->save();

      $roleUser->addPermission('view.profile', true);
      $roleUser->addPermission('update.profile', true);

      $roleUser->addPermission('scenario');
      $roleUser->addPermission('copy.scenario', true);

      $roleUser->addPermission('group');
      $roleUser->addPermission('copy.group', true);

      $roleUser->addPermission('question');
      $roleUser->addPermission('copy.question', true);
    }
}
