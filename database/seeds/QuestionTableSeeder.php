<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('questions')->insert([
          'group_id' => 1,
          'user_id' => 1,
          'company_id' => 1,
          'question' => 'Вопрос 1 для WORLD',
          'answers' => json_encode([
            'Answer 1 for WORLD',
            'Answer 2 for WORLD',
            'Answer 3 for WORLD',
            'Answer 4 for WORLD',
            'Answer 5 for WORLD',
          ]),
          'sort' => 1,
      ]);

      DB::table('questions')->insert([
          'group_id' => 1,
          'user_id' => 1,
          'company_id' => 1,
          'question' => 'Вопрос второй (2) для WORLD',
          'answers' => json_encode([
            'Answer 1 for WORLD 2',
            'Answer 2 for WORLD 2',
            'Answer 3 for WORLD 2',
          ]),
          'sort' => 2,
      ]);

      DB::table('questions')->insert([
          'group_id' => 7,
          'user_id' => 1,
          'company_id' => 1,
          'question' => 'Question for Root',
          'answers' => json_encode([
            'Answer 1 for Root',
            'Answer 2 for Root',
            'Answer 3 for Root',
            'Answer 4 for Root',
          ]),
          'sort' => 1,
      ]);

      DB::table('questions')->insert([
          'group_id' => 8,
          'user_id' => 1,
          'company_id' => 1,
          'question' => 'Question for SubRoot1',
          'answers' => json_encode([
            'Answer 1 for SubRoot1',
            'Answer 2 for SubRoot1',
            'Answer 3 for SubRoot1',
          ]),
          'sort' => 1,
      ]);
    }
}
