<?php

use Illuminate\Database\Seeder;

class ScenarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // сценарии администратора
      DB::table('scenarios')->insert([
          'name' => '[ADMIN]First scenario',
          'user_id' => 1,
          'company_id' => 1,
      ]);

      DB::table('scenarios')->insert([
          'name' => '[ADMIN]Second scenario',
          'user_id' => 1,
          'company_id' => 1,
      ]);


      // сценарии администратора компании
      DB::table('scenarios')->insert([
          'name' => '[COMAPANY_ADMIN]First scenario',
          'user_id' => 2,
          'company_id' => 1,
      ]);

      DB::table('scenarios')->insert([
          'name' => '[COMAPANY_ADMIN]Second scenario',
          'user_id' => 2,
          'company_id' => 1,
      ]);


      // сценариии первого обычного пользователя
      DB::table('scenarios')->insert([
          'name' => '[USER1]First scenario',
          'user_id' => 3,
          'company_id' => 1,
      ]);

      DB::table('scenarios')->insert([
          'name' => '[USER1]Second scenario',
          'user_id' => 3,
          'company_id' => 1,
      ]);


      // сценариии второго обычного пользователя
      DB::table('scenarios')->insert([
          'name' => '[USER2]First scenario',
          'user_id' => 4,
          'company_id' => 1,
      ]);

      DB::table('scenarios')->insert([
          'name' => '[USER2]Second scenario',
          'user_id' => 4,
          'company_id' => 1,
      ]);
    }
}
