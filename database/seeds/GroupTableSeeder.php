<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->cites();
      $this->tree();
      $this->relations();
    }


    // связи
    protected function relations(){
      DB::table('scenario_group')->insert([
          'scenario_id' => 1,
          'group_id' => 1
      ]);

      DB::table('scenario_group')->insert([
          'scenario_id' => 1,
          'group_id' => 7
      ]);

      DB::table('scenario_group')->insert([
          'scenario_id' => 2,
          'group_id' => 1
      ]);
    }

    // генерируем данные в виде дерева
    protected function tree(){
      DB::table('groups')->insert([
          'name' => 'Root',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => true,
          'parent_id' => 0,
          'sort' => 1,
      ]);

      DB::table('groups')->insert([
          'name' => 'SubRoot1',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 7,
          'sort' => 2,
      ]);

      DB::table('groups')->insert([
          'name' => 'SubRoot2',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 7,
          'sort' => 1,
      ]);

      DB::table('groups')->insert([
          'name' => 'SubRoot3',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 7,
          'sort' => 3,
      ]);
    }


    // генерируем данные в виде городов
    protected function cites(){
      // Группа 1
      DB::table('groups')->insert([
          'name' => 'World',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => true,
          'parent_id' => 0,
          'sort' => 1,
      ]);

      DB::table('groups')->insert([
          'name' => 'Russia',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 1,
          'sort' => 1,
      ]);

      DB::table('groups')->insert([
          'name' => 'Moscow',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 2,
          'sort' => 1,
      ]);

      DB::table('groups')->insert([
          'name' => 'Ufa',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 2,
          'sort' => 2,
      ]);

      DB::table('groups')->insert([
          'name' => 'Usa',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 1,
          'sort' => 2,
      ]);


      DB::table('groups')->insert([
          'name' => 'New-York',
          'user_id' => 1,
          'company_id' => 1,
          'is_root' => false,
          'parent_id' => 5,
          'sort' => 1,
      ]);
    }
}
