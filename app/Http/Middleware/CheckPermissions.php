<?php

namespace App\Http\Middleware;

use Closure;

class CheckPermissions
{

    protected $request;
    protected $crud = [
      'create' => ['POST'],
      'read'   => ['GET', 'HEAD', 'OPTIONS'],
      'view'   => ['GET', 'HEAD', 'OPTIONS'],
      'update' => ['PUT', 'PATCH'],
      'delete' => ['DELETE'],
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;

        $actionForCheck = 'view';

        // ищем название action
        foreach ($this->crud as $key => $value) {
          if(in_array($this->request->method(), $value)){
              $actionForCheck = $key;
          }
        }

        $restMethod = $this->parseMethod();
        $strToCheck = $actionForCheck . '.' . $restMethod;

        // dd($name);
        // echo $strToCheck;

        if($request->user()->can($strToCheck)){
            return $next($request);
        } else {
            return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Доступ к данному методу запрещен', 'errors' => []], 401);
        }

    }

    protected function parseMethod() {
        if ( $alias = $this->getAction('protect_alias') ) {
            return $alias;
        }

        $action = $this->request->route()->getActionName();
        $ctrl = preg_match('/([^@]+)+/is', $action, $m) ? $m[1] : $action;
        $name = last(explode('\\', $ctrl));
        $name = str_replace('controller', '', strtolower($name));
        $name = str_singular($name);
        // $action = $this->request->route()->getActionName();
        // if ( preg_match('/@([^\s].+)$/is', $action, $m) ) {
        //     $controller = $m[1];
        //     if ( $controller != 'Closure' ) {
        //         return $controller;
        //     }
        // }
        return $name;
    }

    /**
     * Extract required action from requested route.
     *
     * @param string $key action name
     * @return string
     */
    protected function getAction($key)
    {
        $action = $this->request->route()->getAction();
        return isset($action[$key]) ? $action[$key] : false;
    }
}
