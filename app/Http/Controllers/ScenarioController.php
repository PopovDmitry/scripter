<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
use Validator;

class ScenarioController extends Controller
{
    /**
    * Выдача сценариев
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(Request $request){
      $ids = collect([$request->user()->id]);
      // получаем сценарии компании от их адинистраторов и админов компании
      $admins_id = \App\User::getManagersByRole('company_admin', $request->user()->company_id, ['id'], false);
      if($admins_id){
        foreach ($admins_id->pluck('id')->toArray() as $key => $value) {
            $ids->push($value);
        }
      }

      $admins_id = \App\User::getManagersByRole('administrator', $request->user()->company_id, ['id'], false);
      if($admins_id){
        foreach ($admins_id->pluck('id')->toArray() as $key => $value) {
            $ids->push($value);
        }
      }

      // return $ids->toArray();
      if($ids->count() > 0) {
        $myScenary = \App\Models\Scenario::select(['id', 'name', 'created_at', 'user_id'])
                      ->whereIn('user_id', $ids)
                      ->paginate(env('LIST_PAGINATION', 10));
        if(!$myScenary) {
          return response()->json(['code' => 7, 'message' => 'Нет данных'], 404);
        }

        // помечаем свои сценарии
        $myScenary->map(function($val) use ($request) {
          // $val['isMyScenario'] = ($request->user()->id == $val['user_id']) ? true : false;
          // $val['isEdit']
          if($request->user()->itIsAdmin()){
            $val['is_edit'] = true;
          } else {
            if($val['user_id'] == $request->user()->id){
              $val['is_edit'] = true;
            } else {
              $val['is_edit'] = false;
            }
          }
          unset($val['user_id']);
          return $val;
        });

        return response()->json($myScenary, 200);
      } else {
          return response()->json(['code' => 7, 'message' => 'Нет данных'], 404);
      }
    }

    /**
    * Сосздание сценария
    * @param int $id ID  сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function view($id = 0, Request $request){
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 12,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // проверяем владелец ли сценария пользователь который прендалежит к компании
      $scenario = \App\Models\Scenario::where('id', $id)
                  ->where('company_id', $request->user()->company_id);

      if($scenario->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете просмотреть данную запись.', 'errors' => []], 401);
      }

      $data =  $scenario->first();
      return response()
          ->json($data->only(['name']), 200);

    }

    /**
    * Сосздание сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function create(Request $request){
        $credentials = $request->only(['name']);

        $messages = [
            'name.required' => 'Вы не указали название сценария',
            'name.min' => 'Название сценария должен быть не мение 2 символов',
            'name.max' => 'Пароль не должен превышать больше 100 символов',
        ];

        $validator = Validator::make($credentials, [
            'name' => 'required|min:2|max:100'
        ], $messages);

        if ($validator->fails()) {
            return response()
                ->json([
                    'code' => 1,
                    'message' => 'Validation failed.',
                    'errors' => $validator->errors()
                ], 422);
        }

        $scenario = new \App\Models\Scenario;
        $scenario->name = $request->input('name');
        $scenario->user_id = $request->user()->id;
        $scenario->company_id = $request->user()->company_id;
        if($scenario->save()) {
          return response()->json([
            'result' => true,
            'scenario_id' => $scenario->id
          ]);
        } else {
          return response()
              ->json([
                  'code' => 11,
                  'message' => 'Ошибка создания записи',
                  'errors' => []
              ], 500);
        }
    }

    /**
    * Обновление сценария
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function update($id = 0, Request $request){
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 12,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      $credentials = $request->only(['name']);

      $messages = [
          'name.required' => 'Вы не указали название сценария',
          'name.min' => 'Название сценария должен быть не мение 2 символов',
          'name.max' => 'Пароль не должен превышать больше 100 символов',
      ];

      $validator = Validator::make($credentials, [
          'name' => 'required|min:2|max:100'
      ], $messages);

      if ($validator->fails()) {
          return response()
              ->json([
                  'code' => 1,
                  'message' => 'Validation failed.',
                  'errors' => $validator->errors()
              ], 422);
      }

      // @ TODO сделать проверку на админов

      // проверяем владелец ли сценария пользователь
      $scenario = \App\Models\Scenario::where('id', $id)
                  ->where('user_id', $request->user()->id);
      if($scenario->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете обновить данную запись.', 'errors' => []], 401);
      }

      if($scenario->update($credentials)){
        return response()->json([
          'result' => true
        ]);
      } else {
        return response()
            ->json([
                'code' => 13,
                'message' => 'Ошибка обновления записи',
                'errors' => []
            ], 500);
      }
    }

    /**
    * удаление сценария
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function copy($id = 0, Request $request){
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 12,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // проверяем можно ли скопировать сценарий данной компании
      $scenario = \App\Models\Scenario::where('id', $id)
                  ->where('company_id', $request->user()->company_id);
      if($scenario->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете скопировать данную запись.', 'errors' => []], 401);
      }
      // копируем записть
      $newScenrio = $scenario->first()->replicate();
      $newScenrio->name = '[copy] ' . $newScenrio->name;

      // назначаем владельца копии того, кто иницировал процесс копирования
      $newScenrio->user_id = $request->user()->id;

      if($newScenrio->save()) {

        // получаем связи групп
        $links = \App\Models\ScenarioGroup::where('scenario_id', $id);
        if($links->count() > 0){
            $newScenrio->groups()->attach($links->get()->pluck('group_id'));
        }




        return response()->json([
          'result' => true,
          'scenario_id' => $newScenrio->id
        ]);
      } else {
        return response()
            ->json([
                'code' => 11,
                'message' => 'Ошибка копирования записи',
                'errors' => []
            ], 500);
      }
    }

    /**
    * удаление сценария
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function delete($id = 0, Request $request){
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 12,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // проверяем владелец ли сценария пользователь
      $scenario = \App\Models\Scenario::where('id', $id)
                  ->where('user_id', $request->user()->id);
      if($scenario->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете удалить данную запись.', 'errors' => []], 401);
      }

      if($scenario->delete()){
        return response()->json([
          'result' => true
        ]);
      } else {
        return response()
            ->json([
                'code' => 14,
                'message' => 'Ошибка удалении записи',
                'errors' => []
            ], 500);
      }
    }
}
