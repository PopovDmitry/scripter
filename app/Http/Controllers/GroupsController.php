<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
use Validator;

class GroupsController extends Controller
{

    /**
    * Вывод спсика групп для данной компании
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(Request $request) {
      // $res = \App\Models\Group::getTreesByUser($request->user()->id);
      $res = \App\Models\Group::getTreesByCompany($request->user()->company_id, $request->user());

      if($res){
          return response()->json(\App\Models\Group::buildTree($res), 200);
      } else {
        return response()->json([], 200);
      }



    }

    /**
    * просмотр информации о группе группы
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function view($id = 0, Request $request) {
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 23,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // получение группы только в рамках свое компании
      $group = \App\Models\Group::with('scenarios')->where('id', $id)->where('company_id', $request->user()->company_id);
      if($group->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете обновить данную запись.', 'errors' => []], 401);
      }
      $data = $group->first()->only(['id', 'name', 'is_root', 'sort']);
      $scenarios = $group->first()->scenarios()->get();

      $scenarios = $scenarios->map( function($value) {
          return $value->only(['id', 'name']);
      });

      $data['scenarios'] = $scenarios->toArray();
      return response()
          ->json($data, 200);

    }

    /**
    * Отдача групп сценарию
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function scenario($id = 0, Request $request) {
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 23,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // exit($request->user()->company_id);

      // проверяем можно ли получить сценарий данной компании
      $scenario = \App\Models\Scenario::with('groups')
                  ->where('id', $id)
                  ->where('company_id', $request->user()->company_id);
      if($scenario->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете скопировать данную запись.', 'errors' => []], 401);
      }

      $data = $scenario->first();
      $groups = $data->groups()->get()->pluck('id')->toArray();
      $grs = [];
      $ids = [];
      if(!empty($groups)){
        foreach ($groups as $key => $value) {
          // $res = \App\Models\Group::getTreeByID($value);
          $res = \App\Models\Group::getTreeByIDAndPermissions($value, $request->user());
          // dd($res);
          $tree = \App\Models\Group::buildTree($res);
          if(isset($tree[0])){
              $grs[] = $tree[0];
              $ids[] = (int)$value;
          }
        }
      }

      // return array_values(collect($grs)->sortByDesc('sort')->toArray());

      $result = $data->toArray();
      $result['groups'] = array_values(collect($grs)->sortBy('sort')->toArray());
      // $result['groups'] = $grs;
      $result['ids'] = $ids;

      $result = collect($result)->only(['id', 'name', 'groups', 'ids'])->toArray();

      return response()->json($result, 200);
    }

    /**
    * Создание группы
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function create(Request $request) {
      $inputFileds = $request->only('name', 'parent_id', 'scenario_id');

      $validateMsg = [
        'name.required' => 'Введите имя',
        'name.min' => 'Имя должно быть не мение 2-х символов',
        'name.max' => 'Имя не должно превышать больше 100 символов',

        'parent_id.required' => 'Родительский элемент должен обязательно присутсвовать',
        'parent_id.exists' => 'Родительский элемент не обнаружен',
        'parent_id.numeric' => 'Должно быть числом',

        'scenario_id.required' => 'Родительский элемент должен обязательно присутсвовать',
        'scenario_id.numeric' => 'Должно быть числом',
        'scenario_id.exists' => 'Сценарий не обнаружен',
      ];

      $validateRules = [
        'name' => 'required|max:100',
        'parent_id' => 'required|numeric'
      ];

      if($request->has('parent_id') && $request->input('parent_id') > 0){
          $validateRules['parent_id'] .= "|exists:groups,id";
      }


      if($request->has('scenario_id')){
        $validateRules['scenario_id'] = 'required|numeric|exists:scenarios,id';
      }

      $validator = Validator::make($inputFileds, $validateRules, $validateMsg);

      if ($validator->fails()) {
          return response()
              ->json([
                  'code' => 20,
                  'message' => 'Ошибка валидации',
                  'errors' => $validator->errors()
              ], 422);
      }

      $group = new \App\Models\Group();
      $group->name = $request->input('name');
      $group->parent_id = $request->input('parent_id');
      $group->is_root = ($group->parent_id == 0) ? true : false;
      $group->company_id = $request->user()->company_id;
      $group->user_id = $request->user()->id;

      // получаем максимальное значение из данных по данной ветви
      $sort = \App\Models\Group::where('parent_id', $group->parent_id)->orderBy('sort', 'desc')->first();
      if($sort){
        $group->sort = $sort->sort + 1;
      } else {
        $group->sort = 1; // TODO доделать сортировку
      }


      if($group->save()){
        if($request->has('scenario_id')){
            // проверка возможности привязки группы к сценарию
            $scenario = \App\Models\Scenario::where('id', $request->input('scenario_id'))
                        ->where('company_id', $request->user()->company_id);
            if($scenario->count() == 0){
              $group->delete(); // Удаляем группу
              return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете добавить группу к данному сценарию.', 'errors' => []], 401);
            }

            // создание связи

            // $group->scenarios()->save($scenario->first());
            $group->scenarios()->attach($request->input('scenario_id'));

            // $relation = new \App\Models\ScenarioGroup();
            // $relation->group_id = $group->id;
            // $relation->scenario_id = $request->input('scenario_id');
            // if(!$relation->save()){
            //   $group->delete(); // Удаляем группу
            //   return response()
            //       ->json([
            //           'code' => 22,
            //           'message' => 'Ошибка создания связи',
            //           'errors' => []
            //       ], 500);
            // }
        }

        return response()->json([
          'result' => true,
          'group_id' => $group->id
        ], 200);

      } else {
        return response()
            ->json([
                'code' => 21,
                'message' => 'Ошибка создания группы',
                'errors' => []
            ], 500);
      }
    }


    /**
    * Обновление группы
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function update($id = 0, Request $request) {
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 23,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      $credentials = $request->only(['name', 'parent_id','sort']);

      $messages = [
          'name.required' => 'Вы не указали название сценария',
          'name.min' => 'Название сценария должен быть не мение 2 символов',
          'name.max' => 'Пароль не должен превышать больше 100 символов',
      ];

      $rules = [
          'name' => 'required|max:100'
      ];

      if($request->has('sort')){
        $messages['sort.required'] = 'Не указана сортировка';
        $messages['sort.numeric'] = 'Сортровка должна быть числом';
        $rules['sort'] = 'required|numeric';
      }

      if($request->has('parent_id')){
        $messages['parent_id.required'] = 'Не указан предок';
        $messages['parent_id.numeric'] = 'Предок должен быть числом';
        $rules['parent_id'] = 'required|numeric';

        if($request->input('parent_id') != 0){
          $messages['parent_id.exists'] = 'Предок должен существовать';
          $rules['parent_id'] .= "|exists:groups,id";
          $credentials['is_root'] = false;
        } else {
          $credentials['is_root'] = true;
        }
      }



      $validator = Validator::make($credentials, $rules, $messages);

      if ($validator->fails()) {
          return response()
              ->json([
                  'code' => 24,
                  'message' => 'Validation failed.',
                  'errors' => $validator->errors()
              ], 422);
      }

      // проверяем владелец ли группы пользователь
      if($request->user()->itIsAdmin()) {
          $group = \App\Models\Group::where('id', $id);
      } else {
        $group = \App\Models\Group::where('id', $id)
                    ->where('user_id', $request->user()->id);
        if($group->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете обновить данную запись.', 'errors' => []], 401);
        }
      }

      if($group->update($credentials)){
        return response()->json([
          'result' => true
        ], 200);
      } else {
        return response()
            ->json([
                'code' => 13,
                'message' => 'Ошибка обновления записи',
                'errors' => []
            ], 500);
      }


    }

    /**
    * удаление группы
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function delete($id = 0, Request $request){
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 12,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // проверяем владелец ли сценария пользователь
      if($request->user()->itIsAdmin()) {
        $group = \App\Models\Group::where('id', $id);
      } else {
        $group = \App\Models\Group::where('id', $id)
                    ->where('user_id', $request->user()->id);
        if($group->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете удалить данную группу.', 'errors' => []], 401);
        }
      }

      // удаление каскадом всех других групп
      $grps = \App\Models\Group::getTreeByID($id);
      $ids = collect($grps)->pluck('id')->toArray();

      $groups = \App\Models\Group::with('scenarios')->whereIn('id', $ids);
      // $groups = \App\Models\Group::whereIn('id', $ids);

      foreach ($groups->get() as $key => $value) {
        // dd($value->scenarios()->detach());
        $value->scenarios()->detach();
      }

      // return response()->json([
      //   'result' => true
      // ]);

      if($groups->delete()){
        return response()->json([
          'result' => true
        ]);
      } else {
          return response()
              ->json([
                  'code' => 14,
                  'message' => 'Ошибка удалении записи',
                  'errors' => []
              ], 500);
      }
    }

    /**
    * копирование группы корня без привязки к сценариям
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function copyAsRoot($id = 0, Request $request) {
        $id = intval($id);
        if($id <= 0){
          return response()
              ->json([
                  'code' => 12,
                  'message' => 'Id not found',
                  'errors' => []
              ], 422);
        }

        // получение группы только в рамках свое компании
        $group = \App\Models\Group::where('id', $id)->where('company_id', $request->user()->company_id)->where('is_root', true);
        if($group->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете скопировать данную запись.', 'errors' => []], 401);
        }


        $new_tree_id = \App\Models\Group::copyTreeById($id, $request->user()->id , $request->user()->company_id);

        return response()->json([
          'result' => true,
          'group_id' => $new_tree_id
        ]);
    }


    /**
    * копирование группы(не корневой) без привязки к сценариям
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function copyTreeById($id = 0, Request $request){
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 12,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // получение группы только в рамках свое компании
      $group = \App\Models\Group::where('id', $id)->where('company_id', $request->user()->company_id);
      if($group->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете скопировать данную запись.', 'errors' => []], 401);
      }

      // return $group->first()->id;

      $new_tree_id = \App\Models\Group::copyTreeById($id, $request->user()->id , $request->user()->company_id, $id);

      return response()->json([
        'result' => true,
        'group_id' => $new_tree_id
      ]);
    }

    /**
    * создание связки группы и сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function relation(Request $request) {
      $inputFileds = $request->only('group_id', 'scenario_id');
      $validateMsg = [

        'group_id.required' => 'Группа должна обязательно присутсвовать',
        'group_id.numeric' => 'Должно быть числом',
        'group_id.exists' => 'Группа не обнаружена',

        'scenario_id.required' => 'Сценарий должен обязательно присутсвовать',
        'scenario_id.numeric' => 'Должно быть числом',
        'scenario_id.exists' => 'Сценарий не обнаружен',
      ];

      $validateRules = [
        'group_id' => 'required|numeric|exists:groups,id',
        'scenario_id' => 'required|numeric|exists:scenarios,id'
      ];

      $validator = Validator::make($inputFileds, $validateRules, $validateMsg);

      if ($validator->fails()) {
          return response()
              ->json([
                  'code' => 25,
                  'message' => 'Ошибка валидации',
                  'errors' => $validator->errors()
              ], 422);
      }

      // провекра доступности сценария на связку
      if(!$request->user()->itIsAdmin()) {
        $scenario = \App\Models\Scenario::where('id', $request->input('scenario_id'))
                    ->where('user_id', $request->user()->id);
        if($scenario->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете добавить группу к данному сценарию.', 'errors' => []], 401);
        }
      }


      // добавляем связку
      \App\Models\Group::find($request->input('group_id'))->scenarios()->attach($request->input('scenario_id'));
      return response()->json([
        'result' => true
      ], 200);

    }

    /**
    * удаление связки группы и сценария
    * @param int $id индификатор сценария
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function relationDelete($scenario_id = 0, $group_id = 0, Request $request) {
      $scenario_id = intval($scenario_id);
      if($scenario_id <= 0){
        return response()
            ->json([
                'code' => 23,
                'message' => 'Scenario_id not found',
                'errors' => []
            ], 422);
      }

      $group_id = intval($group_id);
      if($group_id <= 0){
        return response()
            ->json([
                'code' => 23,
                'message' => 'Group_id not found',
                'errors' => []
            ], 422);
      }


      // проверяем админ ли это
      if($request->user()->itIsAdmin()){
        // АДМИН СЦЕНАРИИ
        $scenario = \App\Models\Scenario::where('id', $scenario_id)
                    ->where('company_id', $request->user()->company_id);
        if($scenario->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Хоть вы и админ. но вы не можете отменитьс взяку не из вашей компании', 'errors' => []], 401);
        }

        // АДМИН ГРУППЫ
        $group = \App\Models\Group::where('id', $group_id)
                    ->where('company_id', $request->user()->company_id);
        if($group->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Хоть вы и админ. но вы не можете отменитьс взяку не из вашей компании 2', 'errors' => []], 401);
        }

      } else {
        $scenario = \App\Models\Scenario::where('id', $scenario_id)
                    ->where('user_id', $request->user()->id);
        if($scenario->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете удалить связку группы и данного сценария.', 'errors' => []], 401);
        }

        $group = \App\Models\Group::where('id', $group_id)
                    ->where('user_id', $request->user()->id);

        if($group->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете удалить сценарий из данной группы.', 'errors' => []], 401);
        }
      }

      $group_del = \App\Models\Group::find($group_id);
      $res = $group_del->scenarios()->detach($scenario_id);

      return response()->json([
        'result' => true
      ], 200);
    }

}
