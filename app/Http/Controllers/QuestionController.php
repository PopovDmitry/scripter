<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
use Validator;

class QuestionController extends Controller
{

    /**
    * Отдача списка ко всем дочерним вопросов и ответов к группе
    * @param int $id ID группы
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function all($group_id = 0, Request $request){
      $group_id = intval($group_id);
      if($group_id <= 0){
        return response()
            ->json([
                'code' => 32,
                'message' => 'Group Id not found',
                'errors' => []
            ], 422);
      }

      $group = \App\Models\Group::with('scenarios')->where('id', $group_id)->where('company_id', $request->user()->company_id);
      if($group->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете обновить данную запись.', 'errors' => []], 401);
      }

      // получаем список дочерних групп
      $groups = \App\Models\Group::getTreeByID($group_id);
      $treeView = \App\Models\Group::buildTree($groups, 0);


      $groups = \App\Models\Group::lineTree($treeView);

      foreach ($groups as $key => $group) {
        $questions = \App\Models\Question::where('group_id', $group['id'])
                      ->orderBy('sort', 'desc');
        $data = $questions->get()->map( function ($v) {
          $v['answers'] = json_decode($v['answers'], true);
          $v['display'] = true;
          return $v->only(['id', 'question', 'answers', 'sort', 'display']);
        })->toArray();
        $groups[$key]['questions'] = $data;
        $groups[$key]['display'] = true;
      }

      return response()->json($groups, 200);
    }

    /**
    * Отдача списка вопросов и ответов к группе
    * @param int $id ID группы
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function index($group_id = 0, Request $request) {
      $group_id = intval($group_id);
      if($group_id <= 0){
        return response()
            ->json([
                'code' => 32,
                'message' => 'Group Id not found',
                'errors' => []
            ], 422);
      }

      $questions = \App\Models\Question::where('group_id', $group_id)
                  ->where('company_id', $request->user()->company_id)
                  ->orderBy('sort', 'desc');

      if($questions->count() == 0){
        return response()->json([], 200);
      }

      $data = $questions->get()->map( function ($v) {
        $v['answers'] = json_decode($v['answers'], true);
        return $v->only(['id', 'question', 'answers', 'sort']);
      })->toArray();

      return response()->json($data, 200);

    }

    /**
    * Отдача максимального значения сортировки по данной группе
    *
    * @param int $id ID группы
    * @param \Illuminate\Http\Request
    */
    public function getMaxSort($group_id = 0, Request $request){
      $group_id = intval($group_id);
      if($group_id <= 0){
        return response()
            ->json([
                'code' => 32,
                'message' => 'Group Id not found',
                'errors' => []
            ], 422);
      }
      $questions = \App\Models\Question::where('group_id', $group_id)
                  ->where('company_id', $request->user()->company_id)
                  ->orderBy('sort', 'desc');
      if($questions->count() == 0){
        return response()->json(['sort' => 1], 200);
      }

      return $questions->first()->only('sort');
    }


    /**
    * Отдача одного вопроса и ответов к нему
    * @param int $id ID  вопроса
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function view($id = 0, Request $request) {
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 32,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      $question = \App\Models\Question::where('id', $id)
                  ->where('company_id', $request->user()->company_id);

      if($question->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете получить данную запись.', 'errors' => []], 401);
      }

      $res = $question->first()->only(['id', 'question', 'answers', 'sort']);
      $res['answers'] = json_decode($res['answers'], true);

      return response()->json($res, 200);
    }


    /**
    * Созадние  Вопроса/ответов
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function create(Request $request) {
        $credentials = $request->only(['group_id', 'question', 'answers', 'sort']);
        $messages = [
            'question.required' => 'Вы не указали вопрос',
            'question.min' => 'Вопрос должен быть не мение 2 символов',

            'group_id.required' => 'Вы не указали группу',
            'group_id.numeric' => 'Должно быть числом',
            'group_id.exists' => 'Группа не обноружена',

            'answers.required' => 'Вы не заполнили ответы',
        ];

        if($request->has('sort')){
          $messages['sort.numeric'] = 'Сортировка должна быть числом';
        }

        $validator = Validator::make($credentials, [
            'question' => 'required|min:2',
            'group_id' => 'required|numeric|exists:groups,id',
            'answers' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return response()
                ->json([
                    'code' => 30,
                    'message' => 'Validation failed.',
                    'errors' => $validator->errors()
                ], 422);
        }

        $question = new \App\Models\Question;

        $question->group_id = $request->input('group_id');
        $question->user_id = $request->user()->id;
        $question->company_id = $request->user()->company_id;
        $question->question = $request->input('question');
        $question->answers = json_encode($request->input('answers'));
        $question->sort = $request->has('sort') ? $request->input('sort') : 1;

        if($question->save()){
          return response()->json([
            'result' => true,
            'question_id' => $question->id
          ], 200);
        } else {
          return response()
              ->json([
                  'code' => 31,
                  'message' => 'Ошибка создания записи',
                  'errors' => []
              ], 500);
        }
    }


    /**
    * обновление одного вопроса и ответов к нему
    * @param int $id ID  вопроса
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function update($id = 0, Request $request) {
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 32,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      $credentials = $request->only(['group_id', 'question', 'answers', 'sort']);

      $messages = [
          'question.required' => 'Вы не указали вопрос',
          'question.min' => 'Вопрос должен быть не мение 2 символов',
          //
          // 'group_id.required' => 'Вы не указали группу',
          // 'group_id.numeric' => 'Должно быть числом',
          // 'group_id.exists' => 'Группа не обноружена',

          'answers.required' => 'Вы не заполнили ответы',
      ];

      if($request->has('sort')){
        $messages['sort.numeric'] = 'Сортировка должна быть числом';
      }

      $validator = Validator::make($credentials, [
          'question' => 'required|min:2',
          // 'group_id' => 'required|numeric|exists:groups,id',
          'answers' => 'required',
      ], $messages);

      if ($validator->fails()) {
          return response()
              ->json([
                  'code' => 33,
                  'message' => 'Validation failed.',
                  'errors' => $validator->errors()
              ], 422);
      }

      // проеврка на админа. если НЕ админ, то провяем владелец ли он вопросов и ответов
      if(!$request->user()->itIsAdmin()){
        $question = \App\Models\Question::where('id', $id)
                    ->where('user_id', $request->user()->id);
        if($question->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете обновить данную запись.', 'errors' => []], 401);
        }
      } else {
        $question = \App\Models\Question::where('id', $id);
      }

      $credentials['answers'] = json_encode($credentials['answers']);

      if($question->update($credentials)){
        return response()->json([
          'result' => true
        ]);
      } else {
        return response()
            ->json([
                'code' => 34,
                'message' => 'Ошибка обновления записи',
                'errors' => []
            ], 500);
      }
    }


    /**
    * Удаление одного вопроса и ответов к нему
    * @param int $id ID  вопроса
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function delete($id = 0, Request $request) {
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 35,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      if(!$request->user()->itIsAdmin()) {
        $question = \App\Models\Question::where('id', $id)
                    ->where('user_id', $request->user()->id);
        if($question->count() == 0){
          return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете удалить данную запись.', 'errors' => []], 401);
        }
      } else {
        $question = \App\Models\Question::where('id', $id);
      }

      // return $question->first();

      if($question->delete()){
        return response()->json([
          'result' => true
        ]);
      } else {
        return response()
            ->json([
                'code' => 36,
                'message' => 'Ошибка удалении записи',
                'errors' => []
            ], 500);
      }
    }


    /**
    * Копирование вопроса
    * @param int $id ID вопроса
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function copyQuestion($id = 0, Request $request) {
      $id = intval($id);
      if($id <= 0){
        return response()
            ->json([
                'code' => 35,
                'message' => 'Id not found',
                'errors' => []
            ], 422);
      }

      // проверка копирования и пренадлежность к данной компании
      $q = \App\Models\Question::where('id', $id)->where('company_id', $request->user()->company_id);
      if($q->count() == 0){
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вы не можете скопировать данную запись.', 'errors' => []], 401);
      }

      $resultCopy = \App\Models\Question::copyQuestion($id, $request->user()->id, $request->user()->company_id);

      if($resultCopy != 0){
        return response()->json([
          'result' => true,
          'question_id' => $resultCopy
        ]);
      } else {
        return response()
            ->json([
                'code' => 37,
                'message' => 'Ошибка копирования записи',
                'errors' => []
            ], 500);
      }
    }
}
