<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Validator;
use JWTAuth;
use App\Jobs\SendRegEmail;
use Carbon\Carbon;
use App\Mail\RegMail;

class TokenController extends Controller {

    /**
    * Создаем и возвращаем токен, если пользователь залогинен
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function token(\Tymon\JWTAuth\JWTAuth $auth) {
       if (!Auth::check()) {
           return response()->json(['error' => 'not logged in'], 401);
       }

       // Пользовательские данные отправляем вместе с токеном
       $user = Auth::user();
       $claims = ['name' => $user->name, 'email' => $user->email];

       // Создаем токен из пользователя + добавляем данные
       $token = $auth->fromUser($user, $claims);
       return response()->json(['token' => $token]);
    }


    /**
    * Авторизация
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function login(Request $request) {
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()
                ->json([
                    'code' => 1,
                    'message' => 'Validation failed.',
                    'errors' => $validator->errors()
                ], 422);
        }

        $u = \App\User::where('email', $credentials['email'])->where('is_actived', true);
        if($u->count() == 0){
          return response()->json(['code' => 2, 'message' => 'Аккаунт не активирован', 'errors' => []], 401);
        }

        // автоизуемся
        $token = JWTAuth::attempt($credentials);

        if ($token) {
          // проверка на активацию


            $expdate = JWTAuth::setToken($token)->getPayload()->get('exp') -  Carbon::now()->timestamp;
            return response()->json(['token' => $token, 'exp' => $expdate]);
            // return response()->json(['token' => base64_encode(json_encode(['token' => $token, 'exp' => $expdate]))]);
        } else {
            return response()->json(['code' => 2, 'message' => 'Email или пароль не подходят', 'errors' => []], 401);
        }
    }


    /**
    * Регистрация
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function signup(Request $request){
        $inputFileds = $request->only('email', 'password', 'name', 'company');
        $messages = [
            'email.required' => 'Вы не указали адрес электронной почты',
            'email.email' => 'Не корректный адрес электронной почты',
            'email.unique' => 'Данный адрес электронной почты уже зарегистрован в системе',
            'email.max' => 'В адресе электронной почты количество символов не должно превышать 100 ',

            'password.required' => 'Введите пароль',
            'password.min' => 'Пароль должен быть не мение 6 символов',
            'password.max' => 'Пароль не должен превышать больше 30 символов',

            // 'name.required' => 'Введите имя',
            // 'name.min' => 'Имя должно быть не мение 2-х символов',
            // 'name.max' => 'Имя не должно превышать больше 30 символов',
        ];
        $validator = Validator::make($inputFileds, [
            'email' => 'required|email|unique:users|max:100',
            'password' => 'required|min:6|max:30',
            // 'name' => 'required|min:2|max:30'
        ], $messages);

        if ($validator->fails()) {
            return response()
                ->json([
                    'code' => 3,
                    'message' => 'Signup error',
                    'errors' => $validator->errors()
                ], 422);
        }

        // создаем компанию
        $company = new \App\Models\Company;
        if($request->has('company') && $request->input('company')!= null) {
          $company->name = $request->input('company');
        } else {
          $company->name = '';
        }
        $company->save();

        // создаем учетную запись
        $user = new \App\User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->company_id = $company->id;
        $user->is_actived = false;
        $user->save();
        $user->assignRole('company_admin');

        // прямая отправка письма
        // \Mail::to($user->email)->send(new RegMail($user, $request->input('password')));

        return response()->json(['result' => true]);

        // Постановка в очередь отправки письма с регистрационными данными
        // SendRegEmail::dispatch($user);


        // $res = \Mail::to('pbd.incom@gmail.com')->send(new RegMail(\App\User::find(1), 'dd'));


        // авторизация
        // $token = JWTAuth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
        // if ($token) {
        //   $expdate = JWTAuth::setToken($token)->getPayload()->get('exp') -  Carbon::now()->timestamp;
        //   return response()->json(['token' => $token, 'exp' => $expdate]);
        //   // return response()->json(['token' => base64_encode(json_encode(['token' => $token, 'exp' => $expdate]))]);
        // } else {
        //     return response()->json(['code' => 4, 'message' => 'Попробуйте войти чуть позже', 'errors' => []], 401);
        // }
    }

    /**
    * Выход из системы
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function logout(Request $request) {
        // получаем токен
        $token = JWTAuth::getToken()->get();

        // уничтожаем токен
        $res = JWTAuth::invalidate($token);

        // говорим что все ок
        return response()->json(['result' => true], 200);
    }

    /**
    * Обновление ключа доступа
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function refresh(Request $request) {
      // получаем токен
      $token = JWTAuth::getToken()->get();

      // новый ключ
      $new_token = JWTAuth::refresh($token);

      $expdate = JWTAuth::setToken($new_token)->getPayload()->get('exp') -  Carbon::now()->timestamp;
      // return response()->json(['token' => base64_encode(json_encode(['token' => $new_token, 'exp' => $expdate]))]);
      return response()->json(['token' => $new_token], 200);


    }



    public function email(Request $request){
      $user = \App\User::first();
      return $user->getPermissions();
      // Постановка в очередь отправки письма с регистрационными данными

      // SendRegEmail::dispatch($request->user());
      // var_dump($res);
    }


}
