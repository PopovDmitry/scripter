<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class ProfileController extends Controller
{
  /**
  * отдача профайла
  * @return \Illuminate\Http\JsonResponse
  */
  public function profile(Request $request) {
      $fileds = $request->user()->only('name', 'email', 'is_actived');
      $fileds['company'] = \App\Models\Company::where('id', $request->user()->company_id)->first()->name;
      // $fileds['roles'] = array_values($request->user()->getRoles());
      $fileds['roles'] = \App\User::getUserRoles($request->user()->id)->implode('name', ', ');
      // print_r($fileds['roles']);
      // return \App\Models\Company::find($request->user()->company_id)->get()->first()->name;
      return response()->json($fileds, 200);
  }

  /**
  * Обновление
  * @return \Illuminate\Http\JsonResponse
  */
  public function update(Request $request) {
        $updateData = $request->only('name', 'password', 'company');

        $rulesValidtor = [];

        // если есть имя
        if($request->has('name') && $request->input('name') != null){
          $rulesValidtor['name'] = 'min:2|max:30';
        }

        if($request->has('company') && $request->input('company') != null){
          $rulesValidtor['company'] = 'min:2|max:30';
        }

        // если есть email
        // if($request->has('email')){
        //   if($request->input('email') == $request->user()->only('email')['email']){ // если адрес электронной почты, ровно такойже как и был
        //     $rulesValidtor['email'] = 'required|email|max:100'; // убираем проверку на уникальность
        //   }else{
        //     $rulesValidtor['email'] = 'required|email|unique:users|max:100'; // ставим проверку на уникальность
        //   }
        //
        // }

        // если есть пароль
        if($request->has('password') && $request->input('password') != null ){
          $rulesValidtor['password'] = 'required|min:6|max:30';
        }

        $messages = [
            'email.required' => 'Вы не указали адрес электронной почты',
            'email.email' => 'Не корректный адрес электронной почты',
            // 'email.unique' => 'Данный адрес электронной почты уже зарегистрован в системе',
            'email.max' => 'В адресе электронной почты количество символов не должно превышать 100 ',

            'password.required' => 'Введите пароль',
            'password.min' => 'Пароль должен быть не мение 6 символов',
            'password.max' => 'Пароль не должен превышать больше 30 символов',

            // 'name.required' => 'Введите имя',
            'name.min' => 'Имя должно быть не мение 2-х символов',
            'name.max' => 'Имя не должно превышать больше 30 символов',

            'company.min' => 'Имя должно быть не мение 2-х символов',
            'company.max' => 'Имя не должно превышать больше 30 символов',
        ];

        $validator = Validator::make($updateData, $rulesValidtor, $messages);

        if ($validator->fails()) {
            return response()
                ->json([
                    'code' => 6,
                    'message' => 'Validation failed.',
                    'errors' => $validator->errors()
                ], 422);
        }


        if($request->has('password') && $request->input('password')!= null){
            $updateData['password'] = bcrypt($request->input('password'));
        } else{
          unset($updateData['password']);
        }

        if($request->has('company') && $request->input('company') != null){
          unset($updateData['company']);
          // return \App\Models\Company::find($request->user()->company_id);
          // Обновлении компании
          \App\Models\Company::where('id', $request->user()->company_id)->update(['name' => $request->input('company')]);
        }

        // Обновление пользователя
        \App\User::find($request->user()->id)->update($updateData);



        return response()->json(['result' => true], 200);
  }
}
