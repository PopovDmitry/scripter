<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
use Validator;

class ManagerController extends Controller
{

    /**
    * Выдача менеджеров
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(Request $request){
      $data = \App\User::getManagersByRole('user', $request->user()->company_id);
      if($data){
          if($data->count() != 0) {
            return response()->json($data, 200);
          }
      }

      // return response()->json(['code' => 7, 'message' => 'Нет данных'], 404);
      return response()->json([], 200);
    }

    /**
    * Создание менеджера
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function create(Request $request){
      $inputFileds = $request->only('name', 'email', 'password');

      $messages = [
          'email.required' => 'Вы не указали адрес электронной почты',
          'email.email' => 'Не корректный адрес электронной почты',
          'email.unique' => 'Данный адрес электронной почты уже зарегистрован в системе',
          'email.max' => 'В адресе электронной почты количество символов не должно превышать 100 ',

          'password.required' => 'Введите пароль',
          'password.min' => 'Пароль должен быть не мение 6 символов',
          'password.max' => 'Пароль не должен превышать больше 30 символов',

          'name.required' => 'Введите имя',
          'name.min' => 'Имя должно быть не мение 2-х символов',
          'name.max' => 'Имя не должно превышать больше 30 символов',
      ];
      $validator = Validator::make($inputFileds, [
          'email' => 'required|email|unique:users|max:100',
          'password' => 'required|min:6|max:30',
          'name' => 'required|min:2|max:30'
      ], $messages);

      if ($validator->fails()) {
          return response()
              ->json([
                  'code' => 8,
                  'message' => 'Ошибка валидации',
                  'errors' => $validator->errors()
              ], 422);
      } else{
        // создаем учетную запись
        $user = new \App\User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->company_id = $request->user()->company_id;
        $user->is_actived = true;
        if($user->save()){
          $user->assignRole('user');
          return response()->json(['result' => true]);
        } else {
          return response()
              ->json([
                  'code' => 8,
                  'message' => 'Manager save error',
                  'errors' => []
              ], 422);
        }

      }
    }

    /**
    * Удаление менеджера
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\JsonResponse
    */
    public function delete($email, Request $request){
      // return response()
      //     ->json([
      //         'code' => 10,
      //         'message' => 'not valid email',
      //         'errors' => []
      //     ], 422);

      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        return response()
            ->json([
                'code' => 10,
                'message' => 'not valid email',
                'errors' => []
            ], 422);
      }

      // $data = $request->only('email');
      // return $data['email'];
      $user = \App\User::where('email', $email)
                ->where('company_id', $request->user()->company_id);
      if($user->count() == 1){
          if($user->delete()){
            return response()->json(['result' => true]);
          } else {
            return response()
                ->json([
                    'code' => 9,
                    'message' => 'Manager not deleted',
                    'errors' => []
                ], 422);
          }
      } else {
        return response()->json(['code' => 'INSUFFICIENT_PERMISSIONS', 'message' => 'Вам удаление этого менеджера запрещено', 'errors' => []], 401);
      }


    }

}
