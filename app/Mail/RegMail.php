<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $noHashPassowrd;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $noHashPassowrd = "")
    {
        $this->user = $user;
        $this->noHashPassowrd = $noHashPassowrd;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->view('emails.regmail');
    }
}
