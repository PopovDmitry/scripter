<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Role extends Model
{
    use CrudTrait;

    protected $fillable = [
        'name',
    ];

    public function users() {
        return $this->belongsToMany('\App\Models\User', 'role_user');
    }
}
