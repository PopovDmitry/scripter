<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScenarioGroup extends Model
{
  public function getTable() {
      return 'scenario_group';
  }
}
