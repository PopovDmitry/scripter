<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Scenario extends Model
{

  use CrudTrait;

  protected $fillable = [
      'name', 'user_id', 'company_id'
  ];


  public function groups() {
        return $this->belongsToMany('\App\Models\Group', 'scenario_group');
  }

  public function company(){
    return $this->belongsTo('App\Models\Company');
  }

  public function user(){
    return $this->belongsTo('App\Models\Users');
  }
}
