<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    /**
    * Копирование вопросов из старой группы в новую
    * @param int $old_group_id ID группы откруда надо скопировать
    * @param int $new_group_id ID группы куда надо скопировать
    */
    public static function copyQuestionFronOldGroupToNewGroup($old_group_id, $new_group_id) {
      $groups = self::where('group_id', $old_group_id);
      if($groups->count() != 0){ // есть вопросы
        $groups->get()->each( function ($item) use ($new_group_id) {
          $new_item = $item->replicate();
          $new_item->group_id = $new_group_id;
          $new_item->save();
        });
      }
    }


    /**
    * Копирование поврос
    * @param int $id ID Вопроса который надо скопировать
    * @param mixed $user Данные пользователя который копирует
    * @return int ID нового вопроса
    */
    public static function copyQuestion($id, $user_id, $company_id) {
      // return $user;
      $question = self::find($id);
      if($question){
        $newCopyQuestion = $question->replicate();
        $newCopyQuestion->company_id = $company_id;
        $newCopyQuestion->user_id = $user_id;
        $newCopyQuestion->save();
        return $newCopyQuestion->id;
      } else {
        return 0;
      }
    }
}
