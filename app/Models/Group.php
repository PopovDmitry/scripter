<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model
{

    protected $fillable = [
        'parent_id', 'name', 'user_id', 'company_id', 'is_root', 'sort'
    ];

    public function scenarios() {
        return $this->belongsToMany('\App\Models\Scenario', 'scenario_group');
    }

    public function childs() {
       return $this->hasMany('\App\Models\Groups','parent_id','id') ;
    }

    /**
    * Копирование дерева
    * @param int $id ID корня дерева
    * @param int $user_id ID пользователя
    * @param int $company_id ID компании
    * @param int $parent_id ID родителя
    */
    public static function copyTreeById($id, $user_id, $company_id, $parent_id = 0){
      $tree = self::getTreeByID($id);

      if(!$tree){
        return false;
      }

      $treeStrict = self::buildTree($tree, $tree[0]['parent_id']);

      // dd($tree[0]['parent_id']);
      // dd($tree);

      $newId = self::greate($treeStrict, $tree[0]['parent_id'], $user_id, $company_id);

      return $newId;
    }

    protected static function greate($treeStrict, $parent_id = 0, $user_id = 0, $company_id = 0){

      foreach ($treeStrict as $key => $rows) {
        // создаем запись
        $saveGroup = $rows;
        $saveGroup['parent_id'] = $parent_id;
        $saveGroup['is_root'] = ($parent_id == 0) ? true : false;
        $saveGroup['user_id'] = $user_id;
        $saveGroup['company_id'] = $company_id;
        unset($saveGroup['children']);
        $id = $saveGroup['id'];
        unset($saveGroup['id']);

        $group = self::create($saveGroup);

        // dd($group->id);

        // копирование вопросов
        $questions = \App\Models\Question::copyQuestionFronOldGroupToNewGroup($id, $group->id);

        if(isset($rows['children'])){
            self::greate($rows['children'], $group->id, $user_id, $company_id);
        }
      }

      return $group->id;

    }


    /**
    * Получение списка деревьев конкретного пользователя
    * @param int $user_id ID пользователя
    * @return array одномерный массив
    */
    public static function getTreesByUser($user_id){
      $sql = 'WITH RECURSIVE groupstree as (
                SELECT id, parent_id, name, sort, is_root
                FROM groups
                WHERE user_id = ' . $user_id . '

                UNION

                SELECT groups.id, groups.parent_id, groups.name, groups.sort, groups.is_root
                FROM groups
                  JOIN groupstree
                    ON groups.parent_id = groupstree.id
              )
              SELECT * FROM groupstree ORDER BY sort ASC;
      ';

      $data = DB::select(DB::raw($sql));
      if(!$data){
        return false;
      }
      return collect($data)->map(function($x){ return (array) $x; })->toArray();
    }

    /**
    * получение дерева по его ID с правами доступа
    */
    public static function getTreeByIDAndPermissions($id, $user){
      $sql = 'WITH RECURSIVE groupstree as (
                SELECT id, parent_id, name, sort, is_root, user_id, created_at
                FROM groups
                WHERE id = ' . $id . '
                UNION

                SELECT groups.id, groups.parent_id, groups.name, groups.sort, groups.is_root, groups.user_id, groups.created_at
                FROM groups
                  JOIN groupstree
                    ON groups.parent_id = groupstree.id
              )
              SELECT * FROM groupstree ORDER BY sort ASC, created_at ASC;
      ';

      $data = DB::select(DB::raw($sql));
      if(!$data){
        return false;
      }

      // dd($data);
      // return [$data];

      return collect($data)->map(function($x) use ($user){
        $x = (array) $x;
        // dd($x);
        if($user->itIsAdmin()){
          $x['is_edit'] = true;
        } else {
          if($x['user_id'] == $user->id){
            $x['is_edit'] = true;
          } else {
            $x['is_edit'] = false;
          }
        }
        return $x;
      })->toArray();

      // return collect($data)->map(function($x){ return (array) $x; })->toArray();
    }

    /**
    * Получение списка деревьев конкретного пользователя
    * @param int $company_id ID пользователя
    * @return array одномерный массив
    */
    public static function getTreesByCompany($company_id, $user){
      // dd($company_id);
      $sql = 'WITH RECURSIVE groupstree as (
                SELECT id, parent_id, name, sort, is_root, user_id, created_at
                FROM groups
                WHERE company_id = ' . $company_id . '
                UNION

                SELECT groups.id, groups.parent_id, groups.name, groups.sort, groups.is_root, groups.user_id, groups.created_at
                FROM groups
                  JOIN groupstree
                    ON groups.parent_id = groupstree.id
              )
              SELECT * FROM groupstree ORDER BY sort ASC, created_at ASC;
      ';

      // dd();
      $data = DB::select(DB::raw($sql));
      if(!$data){
        return false;
      }

      // $user->itIsAdmin()
      return collect($data)->map(function($x) use ($user){
        $x = (array) $x;
        // dd($x);
        if($user->itIsAdmin()){
          $x['is_edit'] = true;
        } else {
          if($x['user_id'] == $user->id){
            $x['is_edit'] = true;
          } else {
            $x['is_edit'] = false;
          }
        }
        return $x;
      })->toArray();
    }


    /**
    * Получение дерева по его корневому ID
    * @param int $id ID корня дерева
    * @return array одномерный массив
    */
    public static function getTreeByID($id = 0) {
      $sql = 'WITH RECURSIVE groupstree as (
                SELECT id, parent_id, name, sort, is_root, created_at
                FROM groups
                WHERE id = ' . $id . '
                UNION

                SELECT groups.id, groups.parent_id, groups.name, groups.sort, groups.is_root, groups.created_at
                FROM groups
                  JOIN groupstree
                    ON groups.parent_id = groupstree.id
              )
              SELECT * FROM groupstree ORDER BY sort ASC, created_at ASC;
      ';

      $data = DB::select(DB::raw($sql));
      if(!$data){
        return false;
      }

      return collect($data)->map(function($x){ return (array) $x; })->toArray();
    }


    /**
    * Построение дерева по одномерному массиву
    * @param array $elements одномерный массив
    * @param int $parentId ID Родителя элемента
    * @return array многомерный массив в виде дерева
    */
    public static function buildTree(array $elements, $parentId = 0) {
      $branch = array();

      foreach ($elements as $element) {
          // $element['text'] = $element['name']; // заглушка для фронта
          if ($element['parent_id'] == $parentId) {
              $children = self::buildTree($elements, $element['id']);
              if ($children) {
                  $element['children'] = $children;
              }
              $branch[] = $element;
          }
      }
      return $branch;
  }

  /**
  * Превращенеи многомерного массива дерева в одномерный
  * @param array $tree массив виде дерева
  * @return array
  */
  public static function lineTree(array $tree = []) {
    $returnArray = [];
    if(!empty($tree)){
      foreach ($tree as $row) {

        if(!empty($row['children'])){
          $ins = self::lineTree($row['children']);
        }
        unset($row['children']);
        $returnArray[] = $row;

        if(!empty($ins)){
          foreach ($ins as $insRow) {
            $returnArray[] = $insRow;
          }
        }

        unset($ins);

      }
    }

    return $returnArray;
  }
}
