<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kodeine\Acl\Traits\HasRole;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\DB;

// class User extends Authenticatable implements JWTSubject
class User extends Authenticatable implements JWTSubject
// class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    // use Notifiable;
    // use Notifiable, Authenticatable, CanResetPassword, HasRole;
    use HasRole;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
    * получение ролей пользователя
    * @param int $user_id ID пользоваетеля
    * @return mixed
    */
    public static function getUserRoles($user_id){
      $roles = array_values(self::find($user_id)->getRoles());
      return DB::table('roles')->whereIn('slug', $roles)->get();
    }

    /**
    * Выборка пользователей по роли и компании
    * @param string $slug SLUG Роли
    * @param int $company_id ID компании
    * @param boolean $only Индикатор включения разбиения на страницы
    * @param boolean $paginated Индикатор включения разбиения на страницы
    * @return mixed
    */
    public static function getManagersByRole($slug = '', $company_id = 0, $only = ['id', 'name', 'email'] , $paginate = true){
        // получаем ID роли
        $role_id = DB::table('roles')
          ->select('id')
          ->where('slug', $slug)
          ->first();

        if(!$role_id){
          return false;
        }

        $select = [];
        if(!empty($only)){
          foreach ($only  as $key) {
            $select[] = 'users.' . $key;
          }
        }

        $users = DB::table('users');

        if(!empty($select)){
          $users->select($select);
        }

        $users->join('role_user', 'users.id', '=', 'role_user.user_id')
              ->where('role_user.role_id', $role_id->id)
              ->where('users.company_id', $company_id)
              ->orderBy('users.created_at', 'desc');

        if($users->count() == 0){
          return false;
        }

        if($paginate){
          return $users->paginate(env('LIST_PAGINATION', 10));
        } else {
          return $users->get();
        }
    }

    public function itIsAdmin(){
      $adminFnc = 'is' . ucfirst(env('ROLE_ADMIN', 'administrator'));
      $adminCompanyFnc = 'is' . ucfirst(env('ROLE_COMPANY_ADMIN', 'company_admin'));

      // return $this->$adminCompanyFnc();
      if($this->$adminFnc()){
          return true;
      } elseif ($this->$adminCompanyFnc()){
        return true;
      } else {
        return false;
      }

    }
}
