<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory;

class ApiAuthReg extends TestCase
{
    public $faker;
    public $password;
    public $email;
    public $name;
    public $company;
    public $id;
    public $regUser;

    /**
    * Стартовая функция запускается перед каждым тестом
    * в ней определяем все необходимое для выполнения тестов
    */
    protected function setUp(){
      parent::SetUp();

      // гененируем фейктовые данные
      $this->faker = Factory::create();
      $this->password = 'testingPass';
      // $this->password = $this->faker->password;
      $this->company = $this->faker->company;
      $this->email = $this->faker->email;
      $this->name = $this->faker->firstNameMale;

      // создаем фейкового пользователя в БД
      $this->regUser = factory(\App\User::class)->create();
      // $this->regUser = factory(\App\User::class)->make();
      $this->regUser->assignRole('company_admin');
    }

    /**
    * Функция которая вызыввается по окончаю каждого теста
    */
    protected function tearDown(){
      if(!empty($this->regUser->id)){
          DB::table('users')->where('id', $this->regUser->id)->delete();
          DB::table('companies')->where('id', $this->regUser->company_id)->delete();
      }
    }

    /**
     * Проверка на ошибки
     *
     * @return void
     */
    public function testValidate() {
        // $this->assertTrue(true);
        $this->call('POST', '/api/login', [
            'email' => $this->faker->name,
        ])->assertStatus(422);

        $this->call('POST', '/api/login', [
            'email' => $this->faker->email,
        ])->assertStatus(422);

        $this->call('POST', '/api/login', [
            'password' => $this->faker->email,
        ])->assertStatus(422);

        $this->call('POST', '/api/login', [
            'email' => $this->faker->email,
            'password' => $this->faker->password,
        ])->assertStatus(401);
    }

    /**
     * Проверка на авторизацию
     *
     * @return void
     */
    public function testAuth() {
      $this->assertDatabaseHas('users', [
        'email' => $this->regUser->email
      ]);

      $res = $this->call('POST', '/api/login', [
          'email' => $this->regUser->email,
          'password' => 'secret',
      ])->assertStatus(200);

      $this->assertArrayHasKey('token', $res->original);

      // проверям получилась ли авторизация
      $this->_checkAuth($res->original['token']);
    }

    public function testSignUpOnlyRequery(){
      // отправляем данные на регистрацию
      $res = $this->call('POST', '/api/signup', [
          'email' => $this->email,
          'password' => $this->password
      ])->assertStatus(200);

      $this->assertDatabaseHas('users', [
          'email' => $this->email
      ]);

      // проверяем наличе токена после регистрации(авто вход)
      // $this->assertArrayHasKey('token', $res->original);
      $this->assertArrayHasKey('result', $res->original);

      // проверям получилась ли авторизация
      // $this->_checkAuth($res->original['token']);

      $user = \App\User::where('email',  $this->email)->get()->first();
      // dd($user->company_id);

      // удаляем тестового пользвоателя
      DB::table('users')->where('email', $this->email)->delete();

      // Удаляем компанию
      DB::table('companies')->where('id', $user->company_id)->delete();


    }

    public function testSignUpAllFields(){
        // отправляем данные на регистрацию
        $res = $this->call('POST', '/api/signup', [
            'email' => $this->email,
            'password' => $this->password,
            'name' => $this->name,
            'company' => $this->company,
        ])->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'email' => $this->email
        ]);

        // провекра существования компании в БД
        $this->assertDatabaseHas('companies', [
            'name' => $this->company
        ]);

        // проверяем наличе токена после регистрации(авто вход)
        // $this->assertArrayHasKey('token', $res->original);
        $this->assertArrayHasKey('result', $res->original);

        // проверям получилась ли авторизация
        // $this->_checkAuth($res->original['token']);

        // удаляем тестового пользвоателя
        DB::table('users')->where('email', $this->email)->delete();

        // Удаляем компанию
        DB::table('companies')->where('name', $this->company)->delete();
    }

    /**
    * Проверка на выход
    * @return void
    */
    public function testLogout(){
      // проверка наличи записи в БД
      $this->assertDatabaseHas('users', [
          'email' => $this->regUser->email
      ]);

      // авторизация
      $res = $this->json('POST', '/api/login', [
          'email' => $this->regUser->email,
          'password' => 'secret'
      ])->assertStatus(200);

      $this->_checkAuth($res->original['token']);

      // проверяем выход
      $logoutRes = $this->_callMethod('/api/logout', 'GET');

      $logoutRes->assertStatus(200);
      $logoutRes->assertJson(['result' => true]);

      // пробуем получит данные по токену
      $TestRes = $this->_callMethod('/api/profile', 'GET');
      $TestRes->assertStatus(401);
    }

    /**
    * Обновление токена
    * @return void
    */
    public function testRefreshToken(){
      // проверка наличи записи в БД
      $this->assertDatabaseHas('users', [
          'email' => $this->regUser->email
      ]);

      // авторизация
      $res = $this->json('POST', '/api/login', [
          'email' => $this->regUser->email,
          'password' => 'secret'
      ])->assertStatus(200);

      $oldToken = $res->original['token'];

      // проверяем автоизацию
      $this->_checkAuth($oldToken);

      // запрашиваем обновление пароля
      $resNew = $this->_callMethod('/api/refresh', 'GET');
      $newToken = $resNew->original['token'];

      $this->assertNotEquals($oldToken, $newToken);

      // проверяем автоизацию
      $this->_checkAuth($newToken);

      // выходим
      $logoutRes = $this->_callMethod('/api/logout', 'GET');
      $logoutRes->assertStatus(200);
      $logoutRes->assertJson(['result' => true]);
    }


    /**
    * проверка на валидность токена
    *
    */
    protected function _checkAuth($token = ''){
      $this->assertNotEquals($token, '');
      $this->token = $token;
      $res = $this->_callMethod('/api/profile', 'GET');
      // dd($res->original);
      $res->assertStatus(200);
      $res->assertJsonStructure(['name', 'email', 'company', 'is_actived']);

      // $this->assertArrayHasKey('token', $res->original);
    }

    /**
    * выполнение произвольного метода черех API
    *
    */
    protected function _callMethod($apiMethod = '', $method = 'GET', $params = []) {
      $this->assertNotEquals($apiMethod, '');

      $response = $this->withHeaders([
              'Authorization' => 'Bearer ' . $this->token
      ])->json($method, $apiMethod, $params);

      return $response;
    }
}
