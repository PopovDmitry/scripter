<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory;

class ACL extends TestCase {

    public $token;

    public $companyAdminID = [
      'email' => 'company.admin@scenario.com',
      'password' => '1234567',
    ];

    /**
    * Стартовая функция запускается перед каждым тестом
    * в ней определяем все необходимое для выполнения тестов
    */
    // protected function setUp(){
    //   parent::SetUp();
    //
    // }

    /**
    * Функция которая вызыввается по окончаю каждого теста
    */
    // protected function tearDown(){
    //
    // }

    /**
     * Проверка Функций менеджера
     *
     * @return void
     */
    public function testCompanyAdmin()
    {
        // создаем в БД компанию и менеджера
        $companyAdmin = factory(\App\User::class)->create();
        $companyAdmin->assignRole(env('ROLE_COMPANY_ADMIN', 'company_admin'));

        // авторизация
        $res = $this->json('POST', '/api/login', [
          'email' => $companyAdmin['email'],
          'password' => 'secret',
        ])->assertStatus(200);

        // проверям получилась ли авторизация
        $this->assertArrayHasKey('token', $res->original);

        // проверяем успешность авторизации через API
        $this->token = $res->original['token'];
        $resC1 = $this->_callMethod('/api/profile');
        $resC1->assertStatus(200);

        $old_name = $resC1->original['name'];

        // меняем имя
        $resC2 = $this->_callMethod('/api/profile', 'PUT', [
          'name' => 'testNameUpdate'
        ]);

        $resC2->assertStatus(200);

        // получаем новое имя
        $resC3 = $this->_callMethod('/api/profile');
        $resC3->assertStatus(200);

        $this->assertEquals($resC3->original['name'], 'testNameUpdate');

        // Возвращяаем имя
        $resC4 = $this->_callMethod('/api/profile', 'PUT', [
          'name' => $old_name
        ]);
        $resC4->assertStatus(200);

        // получение компании из БД
        $current_user = \App\User::where('email', $companyAdmin['email'])->first();

        // тестируем создание менеджеров
        $createUser = factory(\App\User::class)->make(['password' => 'secret'])->only(['name', 'email', 'password']);
        $resC5 = $this->_callMethod('/api/manager', 'post', $createUser);
        $resC5->assertStatus(200);
        $resC5->assertJson(['result' => true]);
        $this->assertDatabaseHas('users', [
            'email' => $createUser['email']
        ]);

        // пробуем войти под этим менеджером
        $resU1 = $this->json('POST', '/api/login', [
          'email' => $companyAdmin['email'],
          'password' => 'secret',
        ])->assertStatus(200);

        // проверям получилась ли авторизация созданного пользователя
        $this->assertArrayHasKey('token', $resU1->original);

        // проверяем надичие пользователя в списке
        $resC6 = $this->_callMethod('/api/manager', 'get');
        $this->assertEquals(count($resC6->original->items()), 1);
        $this->assertEquals($resC6->original->first()->email, $createUser['email']);

        // тестируем удаление менеджеров
        $resC7 = $this->_callMethod('/api/manager/' .  $createUser['email'], 'delete');
        $resC7->assertStatus(200);
        $resC7->assertJson(['result' => true]);

        $this->assertDatabaseMissing('users', [
            'email' => $createUser['email']
        ]);

        /*****************************************/
        /********  ТЕСТИРОВАНИЕ СЦЕНАРИЕВ  *******/
        /*****************************************/

        // наполняем тестовые сценарии
        $Scenario = factory(\App\Models\Scenario::class)->create([
          'user_id' => $companyAdmin->id,
          'company_id' => $companyAdmin->company_id
        ]);

        // проверка в бд записи
        $this->assertDatabaseHas('scenarios', [
            'name' => $Scenario->name
        ]);


        // получение сценариев
        $resC8 = $this->_callMethod('/api/scenario/', 'get');
        $resC8->assertStatus(200);
        $this->assertEquals(count($resC8->original->items()), 1);
        $this->assertEquals($resC8->original->first()->name, $Scenario->name);

        // получаем сценарий по его ID через API
        $resView = $this->_callMethod('/api/scenario/view/' . $Scenario->id, 'get');
        // dd($resView->original);
        $resView->assertStatus(200);


        // удаялем созданый сценарий
        $Scenario->delete();

        // создаем сценарий
        $resC9 = $this->_callMethod('/api/scenario/', 'post', [
          'name' => $Scenario->name,
        ]);
        $resC9->assertStatus(200);
        $resC9->assertJson(['result' => true]);

        // проверка в бд записи
        $this->assertDatabaseHas('scenarios', [
            'name' => $Scenario->name
        ]);

        // и сного получаем созданый сценарий
        $resC10 = $this->_callMethod('/api/scenario/', 'get');
        $resC10->assertStatus(200);
        $this->assertEquals(count($resC10->original->items()), 1);
        $this->assertEquals($resC10->original->first()->name, $Scenario->name);

        // пытаемся изменить созданный сценарй
        $testNewNameScenario = 'newScenarioName';
        $resC11 = $this->_callMethod('/api/scenario/' . $resC10->original->first()->id, 'put', [
          'name' => $testNewNameScenario,
        ]);

        $resC11->assertStatus(200);
        $resC11->assertJson(['result' => true]);

        $this->assertDatabaseHas('scenarios', [
            'name' => $testNewNameScenario,
            'id' => $resC10->original->first()->id
        ]);

        // читаем обновленные данные
        $resC12 = $this->_callMethod('/api/scenario/', 'get');
        $resC12->assertStatus(200);
        $this->assertEquals(count($resC12->original->items()), 1);
        $this->assertEquals($resC12->original->first()->name, $testNewNameScenario);


        // попытка обновить сценарий который ему не пренадлежит
        $resC13 = $this->_callMethod('/api/scenario/1', 'put', [
          'name' => $testNewNameScenario,
        ]);
        $resC13->assertStatus(401);

        // тестирование копирования сценария
        $resC14 = $this->_callMethod('/api/scenario/copy/' .  $resC10->original->first()->id, 'get');
        $resC14->assertStatus(200);
        $resC14->assertJson(['result' => true]);

        // проверка наличие в БД
        $this->assertDatabaseHas('scenarios', [
            'id' => $resC14->original['scenario_id']
        ]);

        // тестируем удаление оновного сценария
        $resC15 = $this->_callMethod('/api/scenario/' .  $resC10->original->first()->id, 'delete');
        $resC15->assertStatus(200);
        $resC15->assertJson(['result' => true]);

        // проверка удаления
        $this->assertDatabaseMissing('scenarios', [
            'id' => $resC10->original->first()->id
        ]);

        // тестируем удаление КОПИИ оновного сценария
        $resC16 = $this->_callMethod('/api/scenario/' .  $resC14->original['scenario_id'], 'delete');
        $resC16->assertStatus(200);
        $resC16->assertJson(['result' => true]);

        // проверка удаления
        $this->assertDatabaseMissing('scenarios', [
            'id' =>  $resC14->original['scenario_id']
        ]);

        /*****************************************/
        /**********  ТЕСТИРОВАНИЕ ГРУПП  *********/
        /*****************************************/

        // создание группы
        $tstGroup = factory(\App\Models\Group::class)->make([
          'user_id' => $companyAdmin->id,
          'company_id' => $companyAdmin->company_id
        ]);

        // создание второй группы
        $tstGroup2 = factory(\App\Models\Group::class)->make([
          'user_id' => $companyAdmin->id,
          'company_id' => $companyAdmin->company_id
        ]);

        $Scenario = factory(\App\Models\Scenario::class)->create([
          'user_id' => $companyAdmin->id,
          'company_id' => $companyAdmin->company_id,
        ]);

        // создание группы являсь владельцем
        $gr1 = $this->_callMethod('/api/groups/', 'post', [
          'name' => $tstGroup->name,
          'parent_id' => 0,
          'scenario_id' => $Scenario->id,
        ]);

        $this->assertDatabaseHas('scenario_group', [
            'group_id' => $gr1->original['group_id'],
            'scenario_id' => $Scenario->id
        ]);

        $gr1->assertStatus(200);
        $gr1->assertJson(['result' => true]);


        $this->assertDatabaseHas('groups', [
            'name' => $tstGroup->name
        ]);

        // создание группы со свякой сценария
        // не владелец сценария
        $gr2 = $this->_callMethod('/api/groups/', 'post', [
          'name' => $tstGroup->name,
          'parent_id' => 0,
          'scenario_id' => 1,
        ]);
        $gr2->assertStatus(401); // так как он не является владельцем записи

        $gr3 = $this->_callMethod('/api/groups/', 'post', [
          'name' => $tstGroup->name,
          'parent_id' => $gr1->original['group_id'],
        ]);

        // print_r($gr3->original);

        $gr3->assertStatus(200);
        $gr3->assertJson(['result' => true]);

        $this->assertDatabaseHas('groups', [
            'id' => $gr3->original['group_id']
        ]);


        // получение групп по сценарию
        $grScenario = $this->_callMethod('/api/groups/scenario/' . $Scenario->id, 'get');
        $grScenario->assertStatus(200);
        $grScenario->assertJsonStructure([
          'id',
          'name',
          'groups'
        ]);

        $gr4 = $this->_callMethod('/api/groups/', 'post', [
          'name' => $tstGroup2->name,
          'parent_id' => 0,
        ]);

        $gr4->assertStatus(200);
        $gr4->assertJson(['result' => true]);

        $gr5 = $this->_callMethod('/api/groups/', 'post', [
          'name' => '[sub]' . $tstGroup2->name,
          'parent_id' => $gr4->original['group_id'],
        ]);
        $gr5->assertStatus(200);
        $gr5->assertJson(['result' => true]);

        $gr6 = $this->_callMethod('/api/groups/', 'get');

        // dd($gr6->original);

        $gr6->assertStatus(200);
        $gr6->assertJsonStructure([
          '*' => [
            'id',
            'parent_id',
            'name',
            'children'
          ]
        ]);

        $dataGroupForUpdate = $gr6->original[0];

        $grView = $this->_callMethod('/api/groups/view/' . $gr3->original['group_id'], 'get');
        // dd($grView->original);
        $grView->assertStatus(200);
        $grView->assertJsonStructure([
          'id',
          'name',
          'is_root',
          'sort',
          'scenarios' => [
            '*' => [
              'id',
              'name'
            ]
          ]
        ]);

        // копирование группы
        $grCopy = $this->_callMethod('/api/groups/copyAsRoot/' . $gr1->original['group_id'], 'get');
        $grCopy->assertStatus(200);
        $grCopy->assertJsonStructure(['result', 'group_id']);

        // обновление данных в группе
        $newName = 'NameUpdate';
        $gr7 = $this->_callMethod('/api/groups/' . $dataGroupForUpdate['id'], 'put', [
          'name' => $newName
        ]);
        $gr7->assertStatus(200);
        $gr7->assertJson(['result' => true]);
        $this->assertDatabaseHas('groups', [
            'name' => $newName,
            'id' => $dataGroupForUpdate['id']
        ]);

        // проверка удаления
        $gr8 = $this->_callMethod('/api/groups/' . $dataGroupForUpdate['id'], 'delete');
        // dd($gr8->original);
        $gr8->assertStatus(200);
        $gr8->assertJson(['result' => true]);

        //scenario_group
        $this->assertDatabaseMissing('groups', [
            'id' => $dataGroupForUpdate['id']
        ]);

        // dd($dataGroupForUpdate['id'] . ' - ' . $gr3->original['group_id']);

        // $this->assertDatabaseMissing('groups', [
        //     'id' => $gr3->original['group_id']
        // ]);

        $this->assertDatabaseMissing('scenario_group', [
          'group_id' => $gr3->original['group_id'],
          'scenario_id' => $Scenario->id
        ]);

        $this->assertDatabaseHas('scenarios', [
            'id' => $Scenario->id
        ]);


        // тестирование связки и отвязки сценария к группе
        $Scenario = factory(\App\Models\Scenario::class)->create([
          'user_id' => $companyAdmin->id,
          'company_id' => $companyAdmin->company_id,
        ]);

        $Group = factory(\App\Models\Group::class)->create([
          'user_id' => $companyAdmin->id,
          'company_id' => $companyAdmin->company_id,
        ]);

        $grRelations = $this->_callMethod('/api/groups/relation', 'post', [
            'group_id' => $Group->id,
            'scenario_id' => $Scenario->id,
        ]);

        // dd($grRelations->original);

        $grRelations->assertStatus(200);
        $grRelations->assertJson(['result' => true]);

        $this->assertDatabaseHas('scenario_group', [
          'group_id' => $Group->id,
          'scenario_id' => $Scenario->id
        ]);

        // удаляем связку
        $grRelationsDelete = $this->_callMethod('/api/groups/relation/' . $Scenario->id . '/' . $Group->id, 'delete', [
            'group_id' => $Group->id,
            'scenario_id' => $Scenario->id,
        ]);
        // dd($grRelationsDelete->original);
        $grRelationsDelete->assertStatus(200);
        $grRelationsDelete->assertJson(['result' => true]);

        // $this->assertDatabaseHas('scenario_group', [
        //   'group_id' => $Group->id,
        //   'scenario_id' => $Scenario->id
        // ]);

        $this->assertDatabaseMissing('scenario_group', [
          'group_id' => $Group->id,
          'scenario_id' => $Scenario->id
        ]);

        /*****************************************/
        /***  ТЕСТИРОВАНИЕ ВОПРОСОВ и ОТВЕТОВ  ***/
        /*****************************************/
        $Question = factory(\App\Models\Question::class)->make([
          'user_id' => $companyAdmin->id,
          'company_id' => $companyAdmin->company_id,
          'group_id' => $Group->id,
        ]);

        // создание Вопроса и ответа
        $qAdd = $this->_callMethod('/api/question', 'POST', [
            'group_id' => $Group->id,
            'question' => $Question->question,
            'answers' => $Question->answers
        ]);

        $qAdd->assertStatus(200);
        $qAdd->assertJson(['result' => true]);

        $this->assertDatabaseHas('questions', [
          'question' => $Question->question,
          'id' => $qAdd->original['question_id']
        ]);

        // dd($qAdd->original['question_id']);

        // проверка обновления
        $newName = 'testQuestion';
        $qUpdate = $this->_callMethod('/api/question/' . $qAdd->original['question_id'], 'PUT', [
            'question' => $newName,
            'answers' => $Question->answers
        ]);

        $qUpdate->assertStatus(200);
        $qUpdate->assertJson(['result' => true]);

        $this->assertDatabaseHas('questions', [
          'question' => $newName,
          'id' => $qAdd->original['question_id']
        ]);

        // получение вопроса
        $qView = $this->_callMethod('/api/question/view/' . $qAdd->original['question_id'], 'GET');
        // dd($qView->original);
        $qView->assertStatus(200);
        $qView->assertJsonStructure([
          'id',
          'question',
          'answers',
          'sort'
        ]);

        // получение списка вопросов и ответов
        $qList = $this->_callMethod('/api/question/group/' . $Group->id, 'GET');
        // dd($qList->original);
        $qList->assertStatus(200);
        $qList->assertJsonStructure([
          '*' => [
            'id',
            'question',
            'answers',
            'sort'
          ]
        ]);



        // удаление вопроса и ответов
        $qDelete = $this->_callMethod('/api/question/' . $qAdd->original['question_id'], 'delete');
        // dd($qDelete->original);
        $qDelete->assertStatus(200);
        $qDelete->assertJson(['result' => true]);
        $this->assertDatabaseMissing('questions', [
          'id' => $qAdd->original['question_id']
        ]);



        // удаление данных
        $companyAdmin->delete();
        // $createUser->delete();

    }


    /**
    * Тест обычного пользователя
    *
    * @return void
    */
    // public function testUser(){
    //   // создаем в БД компанию и менеджера
    //   $companyAdmin = factory(\App\User::class)->create();
    //   $companyAdmin->assignRole('user');
    //
    //   // авторизация
    //   $res = $this->json('POST', '/api/login', [
    //     'email' => $companyAdmin['email'],
    //     'password' => 'secret',
    //   ])->assertStatus(200);
    //
    //   // проверям получилась ли авторизация
    //   $this->assertArrayHasKey('token', $res->original);
    //
    //   // проверяем успешность авторизации через API
    //   $this->token = $res->original['token'];
    //   $resC1 = $this->_callMethod('/api/profile');
    //   $resC1->assertStatus(200);
    //
    //   $old_name = $resC1->original['name'];
    //
    //   // меняем имя
    //   $resC2 = $this->_callMethod('/api/profile', 'PUT', [
    //     'name' => 'testNameUpdate'
    //   ]);
    //
    //   $resC2->assertStatus(200);
    //
    //   // получаем новое имя
    //   $resC3 = $this->_callMethod('/api/profile');
    //   $resC3->assertStatus(200);
    //
    //   $this->assertEquals($resC3->original['name'], 'testNameUpdate');
    //
    //   // Возвращяаем имя
    //   $resC4 = $this->_callMethod('/api/profile', 'PUT', [
    //     'name' => $old_name
    //   ]);
    //   $resC4->assertStatus(200);
    //
    //   // получение компании из БД
    //   $current_user = \App\User::where('email', $companyAdmin['email'])->first();
    //
    //   // тестируем создание менеджеров
    //   $createUser = factory(\App\User::class)->make(['password' => 'secret'])->only(['name', 'email', 'password']);
    //   $resC5 = $this->_callMethod('/api/manager', 'post', $createUser);
    //
    //   // указывает, что ему запрещен доступ к данному методу
    //   $resC5->assertStatus(401);
    //   $resC5->assertJson(['code' => 'INSUFFICIENT_PERMISSIONS']);
    //
    //   // попытка получить список менеджеров
    //   $resC6 = $this->_callMethod('/api/manager', 'get');
    //   $resC6->assertStatus(401);
    //   $resC6->assertJson(['code' => 'INSUFFICIENT_PERMISSIONS']);
    // }




    /**
    * выполнение произвольного метода черех API
    *
    */
    protected function _callMethod($apiMethod = '', $method = 'GET', $params = []) {
      $this->assertNotEquals($apiMethod, '');

      $response = $this->withHeaders([
              'Authorization' => 'Bearer ' . $this->token
      ])->json($method, $apiMethod, $params);

      // $response->assertStatus(200);

      return $response;
    }
}
