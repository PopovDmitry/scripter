## Окружение 
### Требование к серверу
 - PHP >= 7.0.0
 - PDO PHP Extension
 - Mbstring PHP Extension
 - Tokenizer PHP Extension
 - XML PHP Extension
 - COMPOSER
 - NGINX >= 1.6
 - PostgreSql >= 9.2
 - NODEJS >= 8.2

###Файл  .env
Данный файл ложится в корень сайта

    APP_NAME=Laravel
	APP_ENV=local
	APP_KEY=base64:RKtlxb2b0vFgvrqR7krlsUQVryxhExJiW6QH6JtiS48=
	APP_DEBUG=true
	APP_LOG_LEVEL=debug
	APP_URL=http://localhost
	
	# данные для соединенния с сервером БД
	DB_CONNECTION=pgsql
	DB_HOST=127.0.0.1
	DB_PORT=5432
	DB_DATABASE=scripter
	DB_USERNAME=homestead
	DB_PASSWORD=secret
	
	BROADCAST_DRIVER=log
	CACHE_DRIVER=file
	SESSION_DRIVER=file
	QUEUE_DRIVER=database
	
	REDIS_HOST=127.0.0.1
	REDIS_PASSWORD=null
	REDIS_PORT=6379
	
	# настройка для отправки почты
	MAIL_DRIVER=mailgun
	MAIL_HOST=smtp.mailtrap.io
	MAIL_PORT=2525
	MAIL_USERNAME=null
	MAIL_PASSWORD=null
	MAIL_ENCRYPTION=null
	
	# секретный ключ для авторизации 
	JWT_SECRET=IuQQEKDCxQsB0O4sN1jOT8D8T81YIxWE
	
	# название ролей для Админа, админа компании и простого пользователя
	ROLE_ADMIN=administrator
	ROLE_COMPANY_ADMIN=company_admin
	ROLE_USER=user
	
	# время жизни сессии при авторизации в минутах
	JWT_TTL=1200


## Установка
```sh
composer install
cd frontend && npm install
```

## Миграции
Накатить миграции
```sh
php artisan migrate
```
Очистить и наполнить БД первоначальными данными
```sh
php artisan migrate:refresh --seed
```
## Build frontend
Билд на локали, так как сервер не вывозит сборку из-за маленького объема оперативы. Делается из папки `frontend`
```sh
ng build --prod --env=prod --base-href "/app/"
```
## Тесты
Запустить все тесты бэкенда на локали
```sh
phpunit --testsuite Unit
```
Запустить все тесты бэкенда на продакшене
```sh
php vendor/phpunit/phpunit/phpunit --testsuite Unit
```
